import React, { Component } from 'react';
import Home from './component/home/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { connect } from 'react-redux';
import Ebus from './component/Ev/Ebus';
import Met from './component/Ev/Met';
import Star from './component/Ev/Star';
import HubSection from './component/startEv/Hub';
import MapSection from './component/startEv/Map';
import RepSection from './component/Ev/Rep';
import { checkFunction } from './store/actions';
import Tone from './component/reserve/Tone';
import Vision from './component/Info/Vision';
import Social from './component/Info/Social';
import Tech from './component/Info/Tech';
import Promotions from './component/Info/Promotions';
import Press from './component/Info/Press';
import Legal from './component/Info/Legal';
import Contact from './component/Info/Contact';
import Torq from './component/reserve/Torq';
import PressOne from './component/press/PressOne';
// import PressMain from './component/press/PressMain';
import PressTwo from './component/press/PressTwo';
import QuestionsAnswers from './component/press/QuestionsAnswers';
import Evintro from './component/press/Evintro';
import Expo3 from './component/press/Expo3';
import Expo2 from './component/press/Expo2';
import Starpress from './component/press/Starpress';
import Metpress from './component/press/Metpress';
import Visionpress from './component/press/Visionpress';
import NewIndex2 from './component/press/NewIndex2';
import EbusPress from './component/press/EbusPress';
import MapPress from './component/press/MapPress';
import HubPress from './component/press/HubPress';
import Ciie from './component/press/Ciie';
import NotFound from './component/NotFound';
import Faq from './component/Info/Faq';

let lastScrollY = 0;


class App extends Component {

  componentDidMount() {
     window.addEventListener('scroll', this.handleScroll, true);
  }


  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  nav = React.createRef();


  showHideArrowIcon = (cls, url, height) => {
    lastScrollY = window.scrollY;
    const pathUrl = window.location.pathname;

    if (document.getElementById(cls) && pathUrl === url) {
      if (lastScrollY > height) {
        document.getElementById(cls).style.display = 'none';
      }
      else {
        document.getElementById(cls).style.display = 'block';
      }
    }
  }

  handleScroll = () => {
    const container = document.querySelector('.star-main-wrapper');
    if (container) {
      lastScrollY = container.scrollTop;
      checkFunction(lastScrollY);
      const pathUrl = window.location.pathname || this.props.pathName;
      if (pathUrl === '/star') {
        this.showHideArrowIcon('star-arrow-page', '/star', 10)
      }
      else if (pathUrl === '/met') {
        this.showHideArrowIcon('met-arrow-page', '/met', 10)
      }
    }
      if(document.getElementById('drop-down-images') && window.scrollY > 0) {

        if(window.scrollY  < 710){
         document.getElementsByClassName('hidden-arrow').length > 0 && document.getElementById('drop-down-images').classList.remove('hidden-arrow')
        }
        else {
         document.getElementById('drop-down-images').classList.add('hidden-arrow')
        }
        }
        if(document.getElementById('drop-down-image')) {
          if(window.scrollY < 35){
           document.getElementsByClassName('hidden-arrow').length > 0 && document.getElementById('drop-down-image').classList.remove('hidden-arrow')
      
          }
          else {
            document.getElementById('drop-down-image').classList.add('hidden-arrow')
          }
          }
  };

  render() {
    const { pathName } = this.props;
    const webRoute = window.location.pathname;

    return (
      <Router>
        <div className={ pathName === 'pressSection' ? "press-root-container" : "root-container"} ref={this.nav}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/star" component={Star} />
            <Route exact path="/met" component={Met} />
            <Route exact path="/about" component={Ebus} />
            <Route exact path="/map" component={MapSection} />
            <Route exact path="/new-index" component={HubSection} />
            <Route exact path="/rep" component={RepSection} />
            <Route exact path="/new-page-7" component={Tone} />
            <Route exact path="/faq" component={Faq} />
            <Route exact path="/vision" component={Vision} />
            <Route exact path="/social" component={Social} />
            <Route exact path="/tech" component={Tech} />
            <Route exact path="/promotions" component={Promotions} />
            <Route exact path="/press" component={Press} />
            <Route exact path="/legal" component={Legal} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/torq" component={Torq} />
            <Route exact path="/121019" component={PressOne} />
            <Route exact path="/12-06-19" component={PressTwo} />
            <Route exact path="/questionsandanswers" component={QuestionsAnswers} />
            <Route exact path="/ciie-2019" component={Ciie} />
            <Route exact path="/evintro" component={Evintro} />
            <Route exact path="/expo3" component={Expo3} />
            <Route exact path="/expo2" component={Expo2} />
            <Route exact path="/starpress" component={Starpress} />
            <Route exact path="/metpress" component={Metpress} />
            <Route exact path="/vision-press" component={Visionpress} />
            <Route exact path="/new-index-2" component={NewIndex2} />
            <Route exact path="/ebus_press" component={EbusPress} />
            <Route exact path="/mappress" component={MapPress} />
            <Route exact path="/hubpress" component={HubPress} />
            <Route component={NotFound} />
          </Switch>


        </div>
      </Router>
    )
  }
}

function mapStateToProps(store) {
  return {
    pathName: store.home.pathName
  }
}

export default connect(mapStateToProps, {})(App);
