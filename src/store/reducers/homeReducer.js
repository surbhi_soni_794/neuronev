const defaultValues = {
    pathName: ''
}

export default function homeReducer(state = defaultValues, action) {
    switch (action.type) {
      case 'CHANGE_ROUTE':
        return { state, pathName: action.payload }
      default:
        return state
    }
  }