import { actionTypes } from "../types"

const defaultValues = {
    pressData: {}
}

const pressReducer = (state = defaultValues, action) => {
    switch (action.type) {
        case actionTypes.pressOne:
            return { state, pressData: action.payload };

        case actionTypes.pressTwo:
            return { state, pressData: action.payload };

        case actionTypes.pressThree:
            return { state, pressData: action.payload };

        case actionTypes.ciie2019:
            return { state, pressData: action.payload };

        case actionTypes.evintro:
            return { state, pressData: action.payload };

        case actionTypes.expo3:
            return { state, pressData: action.payload };

        case actionTypes.expo2:
            return { state, pressData: action.payload };

        case actionTypes.starpress:
            return { state, pressData: action.payload };
            
        case actionTypes.metpress:
            return { state, pressData: action.payload };

        case actionTypes.visionppress:
            return { state, pressData: action.payload };

        case actionTypes.newindex2:
            return { state, pressData: action.payload };

        case actionTypes.ebus_press:
            return { state, pressData: action.payload };

        case actionTypes.mappress:
            return { state, pressData: action.payload };

        case actionTypes.hubpress:
            return { state, pressData: action.payload };

        default:
            return state
    }
}

export default pressReducer