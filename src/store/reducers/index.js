import { combineReducers } from 'redux';
import homeReducer from './homeReducer';
import pressReducer from './pressReducer';

export default combineReducers({
    home:homeReducer,
    press: pressReducer
  })