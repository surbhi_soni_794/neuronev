export const actionTypes = {
    pressOne: '121019',
    pressTwo : '12-06-19',
    pressThree: 'questionsandanswers',
    ciie2019: 'ciie-2019',
    evintro: 'evintro',
    expo3: 'expo3',
    expo2: 'expo2',
    starpress: 'starpress',
    metpress: 'metpress',
    visionppress: 'vision-press',
    newindex2: 'new-index-2',
    ebus_press: 'ebus_press',
    mappress: 'mappress',
    hubpress: 'hubpress'

}