import store from '../store';
import $ from 'jquery';

const { dispatch } = store;

export function changeRoute(path){
    dispatch({
    type: 'CHANGE_ROUTE',
    payload: path
    })
}

export function arrowFunction(lastScrollY){
  if(document.getElementById('drop-down-image')) {
    if(lastScrollY < 35){
     document.getElementsByClassName('hidden-arrow-new').length > 0 && document.getElementById('drop-down-image').classList.remove('hidden-arrow-new')

    }
    else {
       document.getElementById('drop-down-image').classList.add('hidden-arrow-new')
    }
    }
}

export function mapArrow(lastScrollY) {
  if(lastScrollY > 25 && document.getElementsByClassName('maps-arrow').length > 0){
    document.getElementsByClassName('maps-arrow')[0].style.display = 'none'
  }
  if(lastScrollY < 25 && document.getElementsByClassName('maps-arrow').length > 0){
    document.getElementsByClassName('maps-arrow')[0].style.display = 'block'
  }
}


export function checkFunction(lastScrollY) {
    arrowFunction(lastScrollY);
    mapArrow(lastScrollY);
}


export function slidingImages(duration, fadeAmount, durationTime) {

  let intervalFirst = null;
  let intervalSecond = null;
  clearInterval(intervalFirst);
  clearInterval(intervalSecond);

  let images = $("#slideshow img");
  let numImages = images.length;
  let durationMs = duration * durationTime;
  let imageTime = durationMs / numImages; // time the image is visible 
  let fadeTime = imageTime * fadeAmount; // time for cross fading
  let visibleTime = imageTime - (imageTime * fadeAmount * 2);// time the image is visible with opacity == 1
  let animDelay = visibleTime * (numImages - 1) + fadeTime * (numImages - 2); // animation delay/offset for a single image 

  images.each((index, element) => {
      if (index != 0) {
          $(element).css("opacity", "0");
         intervalFirst = setTimeout(() => {
              doAnimationLoop(element, fadeTime, visibleTime, fadeTime, animDelay);
          }, visibleTime * index + fadeTime * (index - 1));
      } else {
        intervalSecond = setTimeout(() => {
              $(element).animate({ opacity: 0 }, fadeTime, () => {
                  setTimeout(() => {
                      doAnimationLoop(element, fadeTime, visibleTime, fadeTime, animDelay);
                  }, animDelay)
              });
          }, visibleTime);
      }
  });
}



export function slidingImage(duration, fadeAmount, durationTime) {

  let intervalFirst = null;
  let intervalSecond = null;
  clearInterval(intervalFirst);
  clearInterval(intervalSecond);

  let images = $("#slideShows1 img");
  let numImages = images.length;
  let durationMs = duration * durationTime;
  let imageTime = durationMs / numImages; // time the image is visible 
  let fadeTime = imageTime * fadeAmount; // time for cross fading
  let visibleTime = imageTime - (imageTime * fadeAmount * 2);// time the image is visible with opacity == 1
  let animDelay = visibleTime * (numImages - 1) + fadeTime * (numImages - 2); // animation delay/offset for a single image 

  images.each((index, element) => {
      if (index != 0) {
          $(element).css("opacity", "0");
         intervalFirst = setTimeout(() => {
              doAnimationLoop(element, fadeTime, visibleTime, fadeTime, animDelay);
          }, visibleTime * index + fadeTime * (index - 1));
      } else {
        intervalSecond = setTimeout(() => {
              $(element).animate({ opacity: 0 }, fadeTime, () => {
                  setTimeout(() => {
                      doAnimationLoop(element, fadeTime, visibleTime, fadeTime, animDelay);
                  }, animDelay)
              });
          }, visibleTime);
      }
  });
}


export function slidingImageAnimation(duration, fadeAmount, durationTime) {

  let intervalFirst = null;
  let intervalSecond = null;
  clearInterval(intervalFirst);
  clearInterval(intervalSecond);

  let images = $("#slideShows2 img");
  let numImages = images.length;
  let durationMs = duration * durationTime;
  let imageTime = durationMs / numImages; // time the image is visible 
  let fadeTime = imageTime * fadeAmount; // time for cross fading
  let visibleTime = imageTime - (imageTime * fadeAmount * 2);// time the image is visible with opacity == 1
  let animDelay = visibleTime * (numImages - 1) + fadeTime * (numImages - 2); // animation delay/offset for a single image 

  images.each((index, element) => {
      if (index != 0) {
          $(element).css("opacity", "0");
         intervalFirst = setTimeout(() => {
              doAnimationLoop(element, fadeTime, visibleTime, fadeTime, animDelay);
          }, visibleTime * index + fadeTime * (index - 1));
      } else {
        intervalSecond = setTimeout(() => {
              $(element).animate({ opacity: 0 }, fadeTime, () => {
                  setTimeout(() => {
                      doAnimationLoop(element, fadeTime, visibleTime, fadeTime, animDelay);
                  }, animDelay)
              });
          }, visibleTime);
      }
  });
}


function doAnimationLoop(element, fadeInTime, visibleTime, fadeOutTime, pauseTime){
  fadeInOut(element,fadeInTime, visibleTime, fadeOutTime ,() => {
    setTimeout(() => {
      doAnimationLoop(element, fadeInTime, visibleTime, fadeOutTime, pauseTime);
    },pauseTime);
  });
}

function fadeInOut( element, fadeIn, visible, fadeOut, onComplete){
  return $(element).animate( {opacity:1}, fadeIn ).delay( visible ).animate( {opacity:0}, fadeOut, onComplete);

}
export const pressComman = (actionTypes, data) => {
  return store.dispatch({
    type: actionTypes,
    payload: data
  })
}