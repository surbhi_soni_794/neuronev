import React from 'react';
import ContactForm from '../Info/ContactForm';
import {slidingImages, slidingImage , slidingImageAnimation} from '../../store/actions/index'
import $ from 'jquery';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

let ToneImagesSlide = 0;
let ToneImagesSlides = 0;
let ToneImagesSlide1 = 0;
let ToneImagesSlide2 = 0;



class Tone extends React.Component {
    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 2500;
            // slidingImages(duration, fadeAmount, durationTime);
            // slidingImage(duration, fadeAmount, durationTime);
            slidingImageAnimation(duration, fadeAmount, durationTime)

        })
    }

  
    componentDidMount() {
        document.title= "T.ONE — Neuron EV";
        this.sildeTypres();
        this.carouselImage();
        this.carousel();
    }


    carousel = () => {
        var x = document.getElementsByClassName("tone-slides-image");
        if (x.length > 0) {
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            ToneImagesSlide++;
            if (ToneImagesSlide > x.length) { ToneImagesSlide = 1 }
            x[ToneImagesSlide - 1].style.display = "block";
            setTimeout(this.carousel, 2000);
        }
    }

    carouselImage = () => {
        var x = document.getElementsByClassName("tone-slides-images");
        if (x.length > 0) {
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            ToneImagesSlides++;
            if (ToneImagesSlides > x.length) { ToneImagesSlides = 1 }
            x[ToneImagesSlides - 1].style.display = "block";
            setTimeout(this.carouselImage, 2000);
        }
    }


    sildeTypres = () => {
        var x = document.getElementsByClassName("tone-slides-images-1");
        if (x.length > 0) {
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            ToneImagesSlide1++;
            if (ToneImagesSlide1 > x.length) { ToneImagesSlide1 = 1 }
            x[ToneImagesSlide1 - 1].style.display = "block";
            setTimeout(this.sildeTypres, 2000);
        }
    }


    render() {
        return (
            <div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <div className="tone-head">
                    T.ONE
                </div>
                <div className="row tone-video tone-video-contain">
                    <iframe src="https://player.vimeo.com/video/368927525?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="contact-conatiner">
                    <div className="contact-form-container">
                        <ContactForm section='tone'/>
                    </div>
                </div>
                <div className="tone-gallery">
                外观展示
                </div>
                <div className="row tone-video tone-video-contain tone-image" >
                    <img src="http://cdn.cn.neuronev.co/assets/images/tone/T+ONE+Exterior1.jpg" alt="exterior" />
                </div>
                <div className="tone-gallery">
                内饰展示
                </div>
                <div className="row tone-video tone-video-contain tone-image" >
                    <img src="http://cdn.cn.neuronev.co/assets/images/tone/T+ONE+Interior1.jpg" alt="interior" />
                </div>
                <div className="container-fluid section-images tone-images">
                    <div className="row">
                        <div className="col-sm-4">
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone1.png" alt="truck-1" /></div>
                            <div>挂车拖车头</div>
                        </div>
                        <div className="col-sm-4">
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone2.png" alt="truck-2" /></div>
                            <div>卡车</div>
                        </div>
                        <div className="col-sm-4">
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone3.png" alt="truck-3" /></div>
                            <div>箱式货车</div>
                        </div>
                    </div>
                </div>
                <div className="row tone-video tone-video-contain">
                    <iframe src="https://player.vimeo.com/video/360315759?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="tone-modular">
                MAP模块化平台
                </div>
                <div className="map-mobility-images container-fluid tone-mobility-images tone-wrapper">
                    <div className="w3-content w3-section">
                        {/* <div className="tone-image-names">T.ONE-V</div> */}
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">MAP-MODULAR PLATFORM</span>
                            <img  alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/map/map12.png" />
                        </div>
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">MAP-MODULAR PLATFORM</span>
                            <img  alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/tone/tone5.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">MAP-MODULAR PLATFORM</span>
                            <img  alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/tone/tone6.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">MAP-MODULAR PLATFORM</span>
                            <img  alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/tone/tone7.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-V</span>
                            <img  alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone8.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-V</span>
                            <img  alt="img-6" src="http://cdn.cn.neuronev.co/assets/images/tone/tone9.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-V</span>
                            <img  alt="img-7" src="http://cdn.cn.neuronev.co/assets/images/tone/tone10.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-V</span>
                            <img  alt="img-8" src="http://cdn.cn.neuronev.co/assets/images/tone/tone11.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-V</span>
                            <img  alt="img-9" src="http://cdn.cn.neuronev.co/assets/images/tone/tone12.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-V</span>
                            <img  alt="img-10" src="http://cdn.cn.neuronev.co/assets/images/tone/tone13.png" />
                        </div>
                       
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-B</span>
                            <img  alt="img-11" src="http://cdn.cn.neuronev.co/assets/images/tone/tone14.png" />
                        </div>
                       
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-C</span>
                            <img  alt="img-12" src="http://cdn.cn.neuronev.co/assets/images/tone/tone15.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-B</span>
                            <img  alt="img-13" src="http://cdn.cn.neuronev.co/assets/images/tone/tone16.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-B</span>
                            <img  alt="img-14" src="http://cdn.cn.neuronev.co/assets/images/tone/tone17.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-L</span>
                            <img  alt="img-15" src="http://cdn.cn.neuronev.co/assets/images/tone/tone18.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-L</span>
                            <img  alt="img-16" src="http://cdn.cn.neuronev.co/assets/images/tone/tone19.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-L</span>
                            <img  alt="img-17" src="http://cdn.cn.neuronev.co/assets/images/tone/tone20.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-L</span>
                            <img  alt="img-18" src="http://cdn.cn.neuronev.co/assets/images/tone/tone21.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-L</span>
                            <img  alt="img-19" src="http://cdn.cn.neuronev.co/assets/images/tone/tone22.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-L</span>
                            <img  alt="img-20" src="http://cdn.cn.neuronev.co/assets/images/tone/tone23.png" />
                        </div>
                       
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-C</span>
                            <img  alt="img-21" src="http://cdn.cn.neuronev.co/assets/images/tone/tone24.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-C</span>
                            <img  alt="img-22" src="http://cdn.cn.neuronev.co/assets/images/tone/tone25.png" />
                        </div>
                        
                        <div className="tone-slides-image">
                            <span className="icon-values-tyre">T.ONE-C</span>
                            <img  alt="img-23" src="http://cdn.cn.neuronev.co/assets/images/tone/tone26.png" />
                        </div>
                       


                    </div>
                </div>
                <div className="row tone-video tone-video-contain" style={{ height: '800px' }}>
                    <iframe src="https://player.vimeo.com/video/359844371?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="tone-passenger">
                三座
                </div>
                <div className="row tone-video tone-video-contain" style={{ height: '800px' }}>
                    <iframe src="https://player.vimeo.com/video/359905242?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="tone-4-passenger">
                四座
                </div>
                <div className="seat-option-tone">
                座椅选择
                </div>
                <div className="container-fluid seat-images">
                    <div className="row text-align-seat">
                        <div className="col-sm-6 image-container-tone">
                            <img src="http://cdn.cn.neuronev.co/assets/images/tone/seat_light+color.png" alt="seat-grey" />
                            <div>浅灰色</div>
                        </div>
                        <div className="col-sm-6 image-container-tone">
                            <img src="http://cdn.cn.neuronev.co/assets/images/tone/tone27.png" alt="seat-grey" />
                            <div>黑色</div>
                        </div>
                    </div>
                </div>
                <div className="map-mobility-images container-fluid  tone-unique-img">
                    <div className= "w3-content w3-section map-sliding-images tone-image-section">
                        {/* <div className="tone-image-names">T.ONE-V</div> */}
                        <div className="tone-slides-images" >
                            <span className="icon-values-tyre">T.ONE-L/2 Passengers</span>
                            <img alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/tone/tone28.png" />
                        </div>
                       
                        <div className="tone-slides-images">
                            <span className="icon-values-tyre">T.ONE-L/4 Passengers</span>
                            <img alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/tone/tone29.png" />
                        </div>
                       
                        <div className="tone-slides-images">
                            <span className="icon-values-tyre">T.ONE-B/4 Passengers</span>
                            <img alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/tone/tone30.png" />
                        </div>
                        
                        <div className="tone-slides-images">
                            <span className="icon-values-tyre">T.ONE-B/5 Passengers</span>
                            <img alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/tone/tone31.png" />
                        </div>
                        
                        <div className="tone-slides-images">
                            <span className="icon-values-tyre">T.ONE-LV/5 Passengers</span>
                            <img alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone32.png" />
                        </div>
                       
                        <div className="tone-slides-images">
                            <span className="icon-values-tyre">T.ONE-LV/6 Passengers</span>
                            <img alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone33.png" />
                        </div>
                        
                    </div>
                </div>
                <div className="interior-light-tone tone-line">
                车内照明
                </div>
                <div className="row tone-video tone-video-contain" style={{ height: '800px' }}>
                    <iframe src="https://player.vimeo.com/video/366583132?background=1&autoplay=1&loop=1"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="interior-light-tone">
                车轮选择
                </div>
                <div className="map-mobility-images container-fluid tone-mobility-images-1 tone-map-mobility-images tone-tyre-uper">
                    <div className="w3-content w3-section map-sliding-images tone-image-section-1">
                        {/* <div className="tone-image-names">金属银</div> */}
                        <div className="tone-slides-images-1">
                            <span className="icon-values-tyre">黑金属/亮色黑</span>
                        <img  alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/tone/tone34.png" />
                        </div>
                        <div className="tone-slides-images-1">
                        <span className="icon-values-tyre">黑金属/亮色黑</span>
                        <img alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/tone/tone35.png" />
                        </div>
                        <div className="tone-slides-images-1">
                        <span className="icon-values-tyre">亮黑色/缎面黑</span>
                        <img alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/tone/tone36.png" />
                        </div>
                        <div className="tone-slides-images-1">
                        <span className="icon-values-tyre">亮黑色/缎面黑</span>
                        <img alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/tone/tone37.png" />
                        </div>
                        <div className="tone-slides-images-1">
                        <span className="icon-values-tyre">金属银</span>
                        <img alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone38.png" />
                        </div>
                        <div className="tone-slides-images-1">
                        <span className="icon-values-tyre">金属银</span>
                        <img alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone39.png" />
                        </div>
                        
                    </div>
                </div>
                <div className="interior-light-tone tone-tyre" style={{ fontSize: '40px' }}>
                汽车材质
                </div>
                <div className="map-mobility-images container-fluid tone-mobility-images-2 tone-lower-sec">
                    <div id="slideShows2" className="map-sliding-images tone-image-section-2">
                        <div className="tone-image-names tone-last">金属银</div>
                        <img className="tone-slides-images-2" alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/tone/tone40.png" />
                        <img className="tone-slides-images-2" alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/tone/tone41.png" />
                        <img className="tone-slides-images-2" alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/tone/tone42.png" />
                        <img className="tone-slides-images-2" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/tone/tone43.png" />
                        <img className="tone-slides-images-2" alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone44.png" />
                        <img className="tone-slides-images-2" alt="img-5" src="http://cdn.cn.neuronev.co/assets/images/tone/tone45.png" />
                    </div>
                </div>

                <div className="tone-thumb-icons">
                    <ul className="leather-wrapper">
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone46.jpg" alt="fab-1" /></div>
                            <div>黑色皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone47.jpg" alt="fab-1" /></div>
                            <div>灰色皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone48.jpg" alt="fab-1" /></div>
                            <div>米黄色皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone49.jpg" alt="fab-1" /></div>
                            <div>黑色透气皮革</div>
                        </li>
                        <li>
                            <div>
                                <img src="http://cdn.cn.neuronev.co/assets/images/tone/tone50.jpg" alt="fab-1" /></div>
                            <div>灰色透气皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone51.jpg" alt="fab-1" /></div>
                            <div>米黄色透气皮革</div>
                        </li>
                        <li>
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone52.jpg" alt="fab-1" /></div>
                            <div>黑色麂皮</div>
                        </li>
                
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone53.jpg" alt="fab-1" /></div>
                            <div>灰色麂皮</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone54.jpg" alt="fab-1" /></div>
                            <div>米黄色麂皮</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone55.jpeg" alt="fab-1" /></div>
                            <div>黑铝</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone56.jpg" alt="fab-1" /></div>
                            <div>银铝</div>
                        </li>
                        <li>
                            <div>
                                <img src="http://cdn.cn.neuronev.co/assets/images/tone/tone57.jpg" alt="fab-1" /></div>
                            <div>深木</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone58.jpeg" alt="fab-1" /></div>
                            <div>灰木</div>
                        </li>
                        <li>
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone59.jpeg" alt="fab-1" /></div>
                            <div>中性木</div>
                        </li>
                    </ul>
                </div>
                <Footer/>
                </main>
            </div>
        )
    }
}

export default Tone