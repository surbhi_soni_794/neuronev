import React from 'react';
import ContactForm from '../Info/ContactForm';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import {slidingImages, slidingImage} from '../../store/actions/index';
import $ from 'jquery';

let ToneImagesSlide = 0;
let ToneImagesSlides = 0;
let ToneImagesSlide1 = 0;
let ToneImagesSlide2 = 0;



class Torq extends React.Component {


    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 900;
            let duration1 = 18;
            let fadeAmount1 = 0.3;
            let durationTime1 = 900;
            slidingImages(duration, fadeAmount, durationTime);
            slidingImage(duration1, fadeAmount1, durationTime1);
        })
    }

    componentDidMount() {
        document.title= "TORQ — Neuron EV";
        this.sildeTypres();
        this.sildeInterior();
    }

    sildeTypres = () => {
        var x = document.getElementsByClassName("tone-slides-images-1");
        if (x.length > 0) {
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            ToneImagesSlide1++;
            if (ToneImagesSlide1 > x.length) { ToneImagesSlide1 = 1 }
            x[ToneImagesSlide1 - 1].style.display = "block";
            setTimeout(this.sildeTypres, 2000);
        }
    }
    sildeInterior = () => {
        var x = document.getElementsByClassName("tone-slides-images-2");
        if (x.length > 0) {
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            ToneImagesSlide2++;
            if (ToneImagesSlide2 > x.length) { ToneImagesSlide2 = 1 }
            x[ToneImagesSlide2 - 1].style.display = "block";
            setTimeout(this.sildeInterior, 2000);
        }
    }



    render() {
        return (
            <div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                 <div className="tone-head">
                    TORQ
                </div>
              <div className="row tone-video tone-video-contain">
                    <img style={{ width: '100%' }} src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566095234-QPTBWGQ2FHZG7P1QM6B3/ke17ZwdGBToddI8pDm48kDP7kJAFAah05H3ChyNlHSF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0it-y9AhwOjL4KDx2eRBQLytfL3JLJfhKK4NcRL0Ex0VjVofBjedgzIkUlRP_S2gPA/TORQ_EXT_1.png?format=2500w" alt="torq-image" />
                </div>
                <div className="row tone-video tone-video-contain" style={{ height: '780px' }}>
                    <iframe src="https://player.vimeo.com/video/366366581?background=1&autoplay=1&loop=1"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="contact-conatiner">
                    <div className="contact-form-container">
                        <ContactForm section='tone'/>
                    </div>
                </div>
                 <div className="tone-gallery">
                 外观展示
                </div>
                <div id="slideshow" className="map-mobility-images container-fluid tone-mobility-images" style={{ display: 'block'}}>
                    <div className="w3-content w3-section map-sliding-images tone-image-section">
                        <img className="tone-slides-images" alt="img-1" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566175168-GAYHZTCUQZO6NJ0K5WUI/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/N_semi.jpg?format=1500w" />
                        <img className="tone-slides-images" alt="img-2" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566173996-8A92K7T2TETNB8KRFIHO/ke17ZwdGBToddI8pDm48kDbCtFRx-v5aRMPXoqI0kiV7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0p4Wyba38KfG317vYluk45_rZHvAFGj27iajNXAGpU6hf8LmNTWoFs74qPiAswWB0Q/N_semi2.jpg?format=1500w" />
                        <img className="tone-slides-images" alt="img-3" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566177219-739GWJAPRANB9XO9PMLW/ke17ZwdGBToddI8pDm48kCX-V5vw-8h9IBXN10-_8XN7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0p4Wyba38KfG317vYluk45_zZdtnDCZTLKcP2mivxmYi50xvY5saIGKMgOza9mH4XA/Neuron_TORQ_Ext1.jpg?format=1500w" />
                        <img className="tone-slides-images" alt="img-4" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566178203-7TPUJ771T2AGAFK9MYXG/ke17ZwdGBToddI8pDm48kCX-V5vw-8h9IBXN10-_8XN7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0p4Wyba38KfG317vYluk45_zZdtnDCZTLKcP2mivxmYi50xvY5saIGKMgOza9mH4XA/Neuron_TORQ_Ext2.jpg?format=1500w" />
                        <img className="tone-slides-images" alt="img-5" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566179902-T8ZTND55M69KFHWTTLW9/ke17ZwdGBToddI8pDm48kCX-V5vw-8h9IBXN10-_8XN7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0p4Wyba38KfG317vYluk45_zZdtnDCZTLKcP2mivxmYi50xvY5saIGKMgOza9mH4XA/Neuron_TORQ_Ext3.jpg?format=1500w" />
                    </div>
                </div>
                <div className="tone-gallery">
                内饰展示
                </div>
                <div id="slideShows1" className="map-mobility-images container-fluid tone-mobility-images" style={{ display: 'block'}}>
                    <div className="w3-content w3-section map-sliding-images tone-image-section torq-image">
                        <img className="tone-slides-image" alt="img-1" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566402327-KV24N0D6HR3L1QCJR8JO/ke17ZwdGBToddI8pDm48kDP7kJAFAah05H3ChyNlHSF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0it-y9AhwOjL4KDx2eRBQLytfL3JLJfhKK4NcRL0Ex0VjVofBjedgzIkUlRP_S2gPA/Semi_T__in_2.png?format=1500w" />
                        <img className="tone-slides-image" alt="img-2" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566398717-XJ1IKAZY4V2ELGOIH1Z7/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/semi_interior7.jpg?format=1500w" />
                        <img className="tone-slides-image" alt="img-3" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566399989-G0V5771FYK6NLMOESY9X/ke17ZwdGBToddI8pDm48kDP7kJAFAah05H3ChyNlHSF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0mxU0godxi02JM9uVemPLqy9uOsj-q5bCkZmGZoQ7_oEh9b9-wZU0EZkSsdXFC8ujw/Semi_T__in_1.jpg?format=1500w" />
                        <img className="tone-slides-image" alt="img-4" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566400887-H401ILSU0TQUVBRFUW06/ke17ZwdGBToddI8pDm48kLLdTrIXSSIzUJqDYVcFamV7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0g9xiYCO_4ze-uEG5pWlE5MGjYYRXKQ4NBWiCs8p8cC7ITCQo2ZULjCYhSXGqM62sQ/Semi_T__in_5.jpg?format=1500w" />
                        <img className="tone-slides-image" alt="img-5" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566402154-C39WG6F9KE0CKGIVW6YQ/ke17ZwdGBToddI8pDm48kDP7kJAFAah05H3ChyNlHSF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0mxU0godxi02JM9uVemPLqy9uOsj-q5bCkZmGZoQ7_oEh9b9-wZU0EZkSsdXFC8ujw/Semi_T__in_2.jpg?format=1500w" />
                        <img className="tone-slides-image" alt="img-6" src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566406942-BU2HHDQPHZ3WDJ3Y6CG8/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0p52bY8kZn6Mpkp9xtPUVLhvLurswpbKwwoDWqBh58NLxQZMhB36LmtxTXHHtLwR3w/e-TRUCK188.png?format=1500w" />
                    </div>
                </div>
                <div className="row tone-video tone-video-contain" style={{ height: '800px' }}>
                    <iframe src="https://player.vimeo.com/video/366370821?background=1&autoplay=1&loop=1"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="tone-gallery interior-torq">
                <span>车内照明</span>
                </div>
                <div className="row tone-video tone-video-contain" style={{ height: '800px', marginTop: '0' }}>
                    <iframe src="https://player.vimeo.com/video/366555461?background=1&autoplay=1&loop=1"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                </div>
                <div className="interior-light-tone torq-text-wrapper">
                汽车材质
                </div>
                <div className="row tone-video tone-video-contain">
                    <img style={{ width: '100%' }} src="https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575566766138-GWGNDHDLHINHBR7WIEAJ/ke17ZwdGBToddI8pDm48kDP7kJAFAah05H3ChyNlHSF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0it-y9AhwOjL4KDx2eRBQLytfL3JLJfhKK4NcRL0Ex0VjVofBjedgzIkUlRP_S2gPA/Semi_T__in_2.png?format=1500w" alt="torq-image" />
                </div>
                <div style={{ marginTop: '3%'}}>
                <ul className="leather-wrapper">
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone46.jpg" alt="fab-1" /></div>
                            <div>黑色皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone47.jpg" alt="fab-1" /></div>
                            <div>灰色皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone48.jpg" alt="fab-1" /></div>
                            <div>米黄色皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone49.jpg" alt="fab-1" /></div>
                            <div>黑色透气皮革</div>
                        </li>
                        <li>
                            <div>
                                <img src="http://cdn.cn.neuronev.co/assets/images/tone/tone50.jpg" alt="fab-1" /></div>
                            <div>灰色透气皮革</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone51.jpg" alt="fab-1" /></div>
                            <div>米黄色透气皮革</div>
                        </li>
                        <li>
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone52.jpg" alt="fab-1" /></div>
                            <div>黑色麂皮</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone53.jpg" alt="fab-1" /></div>
                            <div>灰色麂皮</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone54.jpg" alt="fab-1" /></div>
                            <div>米黄色麂皮</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone55.jpeg" alt="fab-1" /></div>
                            <div>黑铝</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone56.jpg" alt="fab-1" /></div>
                            <div>银铝</div>
                        </li>
                        <li>
                            <div>
                                <img src="http://cdn.cn.neuronev.co/assets/images/tone/tone57.jpg" alt="fab-1" /></div>
                            <div>深木</div>
                        </li>
                        <li>
                            <div className="leather-wrapper-img"><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone58.jpeg" alt="fab-1" /></div>
                            <div>灰木</div>
                        </li>
                        <li>
                            <div><img src="http://cdn.cn.neuronev.co/assets/images/tone/tone59.jpeg" alt="fab-1" /></div>
                            <div>中性木</div>
                        </li>
                    </ul>
                </div>
                <Footer/>
                </main>
            </div>
        )
    }
}

export default Torq