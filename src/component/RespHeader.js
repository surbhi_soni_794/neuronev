import React, { Component } from 'react';
import $ from 'jquery';
import { withRouter } from 'react-router-dom';
import { changeRoute } from '../store/actions';

class RespHeader extends Component {
    state = {
        isNavShow: false,
        isEvClick: false,
        isReserveClick: false,
        isInfoClick: false,
        isPressClick: false
    }

    openNav = () => {
        this.setState({ isNavShow: true })
    }

    closeNav = () => {
        this.setState({
            isNavShow: false,
            isEvClick: false,
            isReserveClick: false,
            isInfoClick: false,
            isPressClick: false
        })
    }
    toggalNav = () => {
        this.setState({ isNavShow: !this.props.isNavShow })
    }

    onGoBack = () => {
        this.setState({
            isEvClick: false,
            isReserveClick: false,
            isInfoClick: false,
            isPressClick: false,
            isNavShow: true
        })
    }

    onSectionClick = (name) => {
        if (name === 'Reserve') {
            this.setState({ isReserveClick: true, isNavShow: false })
        }
        else if (name === 'Ev') {
            this.setState({ isEvClick: true, isNavShow: false })
        }
        else if (name === 'Info') {
            this.setState({ isInfoClick: true, isNavShow: false })
        }
        else if (name === 'Press') {
            this.setState({ isPressClick: true, isNavShow: false })
        }

    }


    redirectToRoute = (routeName) => {
        changeRoute(routeName);
        this.props.history.push(routeName)
    }

    render() {
        const { isEvClick, isReserveClick, isInfoClick, isPressClick } = this.state;

        return (
            <div>
                <div className="responsive-wrapper">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="responsive-nav-wrapper responsivenav-dark">

                                {this.state.isNavShow || isEvClick || isReserveClick || isInfoClick || isPressClick ? <div className="closebtn" onClick={this.closeNav} >&times;</div> : null}
                            <div className="header-img-wrapper">
                                <a className="navbar-brand-wrappper" href="#">
                                    <img alt="logo" onClick={() => this.redirectToRoute('/')} className="nav-img-branding-logo" src="assets/images/logo.png" />
                                </a>
                            </div>
                                {this.state.isNavShow ?
                                    <div id="mySidenav" className="sidenav first-sidenav">
                                        {/* home(neuron) section open */}
                                        <span onClick={() => this.redirectToRoute('/')} className="neu-wrapper">牛仁</span>
                                        {/* home(neuron) section closed */}


                                        {/* ev section open */}
                                            <span className="ev-wrapper" onClick={() => this.onSectionClick('Ev')}>电动汽车</span>
                                        {/* ev section closed */}


                                        {/* reserve section open */}
                                            <span className="ev-wrapper" onClick={() => this.onSectionClick('Reserve')}>预定</span>
                                        {/* reserve section closed */}

                                        {/* info section open */}
                                            <span className="ev-wrapper" onClick={() => this.onSectionClick('Info')}>品牌信息</span>
                                        {/* info section closed */}


                                        {/* press section open */}
                                            <span className="ev-wrapper" onClick={() => this.onSectionClick('Press')}>新闻中心</span>
                                        {/* press section closed */}
                                    </div>
                                    : null}
                                <span className="toogel-wrapper" onClick={this.toggalNav}>&#9776;</span>
                            </nav>
                        </div>
                    </div>
                </div>

                {isEvClick ?
                    <div className="ev-sidebar-wrapper">
                        <div className="slide">
                            <span onClick={this.onGoBack} className="before-wrapper">BACK</span>
                            <span onClick={() => this.redirectToRoute('/star')}>Star</span>
                            <span onClick={() => this.redirectToRoute('/met')}>Met</span>
                            <span onClick={() => this.redirectToRoute('/about')}>Ebus</span>
                            <span onClick={() => this.redirectToRoute('/map')}>Map</span>
                            <span onClick={() => this.redirectToRoute('/new-index')}>HUb</span>
                            <span onClick={() => this.redirectToRoute('/rep')}>Rep</span>
                        </div>
                    </div>
                    : null
                }
                {
                    isReserveClick ?
                        <div onClick={this.onSectionClick} className="reverse-sidebar-wrapper">
                            <div className="slide">
                                <span onClick={this.onGoBack} className="before-wrapper">BACK</span>
                                <span onClick={() => this.redirectToRoute('/new-page-7')}>T.ONE</span>
                                <span onClick={() => this.redirectToRoute('/torq')}>TORQ</span>

                            </div>
                        </div>

                        : null}
                {
                    isInfoClick ?
                        <div className="info-sidebar-wrapper">
                            <div className="slide">
                                <span onClick={this.onGoBack} className="before-wrapper">BACK</span>
                                <span onClick={() => this.redirectToRoute('/faq')}>FAQ</span>
                                <span onClick={() => this.redirectToRoute('/vision')}>关于我们</span>
                                <span onClick={() => this.redirectToRoute('/social')}>社会</span>
                                <span onClick={() => this.redirectToRoute('/tech')}>科技</span>
                                <span onClick={() => this.redirectToRoute('/promotions')}>促销活动</span>
                                <span onClick={() => this.redirectToRoute('/press')}>大事件</span>
                                <span onClick={() => this.redirectToRoute('/legal')}>法律</span>
                                <span onClick={() => this.redirectToRoute('/contact')}>联系我们</span>
                            </div>
                        </div>
                        : null}
                {
                    isPressClick ?
                        <div className="press-sidebar-wrapper">
                            <div className="slide">
                                <span onClick={this.onGoBack} className="before-wrapper">BACK</span>
                                <span onClick={() => this.redirectToRoute('/121019')}>12/10/19</span>
                                <span onClick={() => this.redirectToRoute('/12-06-19')}>12/06/19</span>
                                <span onClick={() => this.redirectToRoute('/questionsandanswers')}>11/22/19</span>
                                <span onClick={() => this.redirectToRoute('/ciie-2019')}>11/20/19</span>
                                <span onClick={() => this.redirectToRoute('/evintro')}>11/06/19</span>
                                <span onClick={() => this.redirectToRoute('/expo3')}>10/21/19</span>
                                <span onClick={() => this.redirectToRoute('/expo2')}>10/07/19</span>
                                <span onClick={() => this.redirectToRoute('/starpress')}>10/01/19</span>
                                <span onClick={() => this.redirectToRoute('/metpress')}>10/01/19</span>
                                <span onClick={() => this.redirectToRoute('/vision-press')}>09/19/19</span>
                                <span onClick={() => this.redirectToRoute('/new-index-2')}>08/27/19</span>
                                <span onClick={() => this.redirectToRoute('/ebus_press')}>08/08/19</span>
                                <span onClick={() => this.redirectToRoute('/mappress')}>07/08/19</span>
                                <span onClick={() => this.redirectToRoute('/hubpress')}>05/10/19</span>
                            </div>
                        </div>
                        : null}
            </div>
        )
    }
}
export default withRouter(RespHeader)
