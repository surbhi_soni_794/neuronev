import React from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import {slidingImages} from '../../store/actions/index';
import $ from 'jquery';

class RepSection extends React.Component { 
    
    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 1200;
            slidingImages(duration, fadeAmount, durationTime);
            
        })
    }
    
    componentDidMount(){
        document.title= "REP - Neuron EV";
        if(document.body.querySelector('.star-main-wrapper'))
        document.body.querySelector('.star-main-wrapper').addEventListener('scroll', this.aosInit);
          }

          aosInit(){
            window.AOS.init({
              duration: 2000,
            });
          }

          goToSection =() => {
            var elmnt = document.getElementById("map-video");
            elmnt.scrollIntoView();
          }

          goToBottom =() => {
            var elmnt = document.getElementById("map-video");
            elmnt.scrollIntoView();
          }
          goToTop =() => {

            var elmnt = document.getElementById("top-id");
            elmnt.scrollIntoView();
          }

          enterFirst = () => {

            if(document.getElementsByClassName('first-val').length > 0)
             document.getElementsByClassName('first-val')[0].style.visibility = 'initial';
          }
          enterSecond = () => {

            if(document.getElementsByClassName('second-val').length > 0)
             document.getElementsByClassName('second-val')[0].style.visibility  = 'initial';
              
        }
        leaveFirst = () => {
            if(document.getElementsByClassName('first-val').length > 0 )
             document.getElementsByClassName('first-val')[0].style.visibility = 'hidden';
        }

        leaveSecond = () => {
            if(document.getElementsByClassName('second-val').length > 0 )
              document.getElementsByClassName('second-val')[0].style.visibility = 'hidden';
        }


    render() {
        return (
            <div className="rep-container-section">
                <div className="cursor-pointing">
                <div className="dash-container"   onClick={this.goToTop}><span  className="dash-value first-val">1</span><span onMouseEnter={this.enterFirst}   onMouseLeave={this.leaveFirst}  className="map-dash"></span></div>
                <div className="dash-container"  onClick={this.goToBottom}><span className="dash-value second-val">2</span> <span onMouseEnter={this.enterSecond}  onMouseLeave={this.leaveSecond} className="map-dash"></span></div>                
                </div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <div id="top-id" className="parallax-image-rep">
                    <div className="parallax-image-content-rep">
                        <div>可再生能源交通的新未来
                        </div>
                        <div  className='rep-arrow'>
                            <img id="drop-down-image" className="arrow-rep" onClick={this.goToSection} src="http://cdn.cn.neuronev.co/assets/images/arrow1.png" alt="arrow" />
                        </div>
                    </div>
                </div>
                <section className="rep-parallax-img-wrapper parallax bg1">  </section>
                <div className="mapping-section-rep">
                    <div id="map-video"><div className="reps-wrapper">REP</div><div style={{ fontSize: '13px'}}>可再生能源广场</div></div>
                </div>

                <div  className="container-fluid">
                 <div className="map-video">
                    <div className="row">
                        <img src="http://cdn.cn.neuronev.co/assets/images/rep/rep1.jpg" id="newly-id" data-aos="fade-up" data-aos-duration="3000" style={{width: '100%'}}  alt="rep-image"/>
                    </div>
                </div>
                </div>
                 <div className="rep-container container">
                     <div className="row">
                            <div className="col-sm-2"></div>
                            <div className="col-sm-8 rep-section-data">
                            REP是一个引人注目集技术与休闲于一体的服务中心，人们在这里一边享受时光，一边为他们的电动汽车充电。这是一个自身可持续的能源中心，可连接其他“REP”，建立模块化易于扩展。
                            </div>
                            <div className="col-sm-2"></div>
                     </div>
                 </div>

                 <div className="hub-image-one container-fluid">
                    <img src="http://cdn.cn.neuronev.co/assets/images/rep/rep2.jpg" alt="img-one" />
                </div>


                <div className="container-fluid section-divider-map" style={{ marginBottom: '2%'}}>
                    <div className="row">
                        <div className="col-sm-6">
                            <img id="rep-image-one" src="http://cdn.cn.neuronev.co/assets/images/rep/rep3.jpg" data-aos="fade-right" style={{ height: '100%'}} alt="img-one" />
                            <div></div>
                        </div>
                        <div className="col-sm-6">
                            <img id="rep-image-two" src="http://cdn.cn.neuronev.co/assets/images/rep/rep4.jpg" data-aos="fade-left" alt="img-one" />
                        </div>
                    </div>
                </div>

                <div className="container-fluid rep-con">
                    <div className="row">
                        <div className="col-sm-6 smart-rep">
                        智能充电桩
                        </div>
                        <div className="col-sm-6">
                            <div className="container-fluid">
                                <div className="row">
                                <div className="col-sm-8" style={{ fontSize: '12px'}}>
                                REP通过多种方式提供能源：太阳能电池板发电，燃料电池存储的电能和发电机的无线快速充电。 它还可以提供维护服务和电池更换。

                                    </div>
                                    <div className="col-sm-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div className="map-mobility-images container-fluid hub-mobility-images rep-images-bottom">
                    <div  id="slideshow"  className="map-sliding-images rep-image-section rep-secconten">
                        <img className="map-slides-image" alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/rep/rep5.jpg" />
                        <img className="map-slides-image" alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/rep/rep6.jpg" />
                        <img className="map-slides-image" alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/rep/rep7.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/rep/rep8.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/rep/rep9.jpg" />
                    </div>
                </div> 

                <div className=" container rep-content-bottom">
                    <div className="row">
                        <div className="col-sm-12 rep-container-2">                            
                        该中心的特点是将食品、饮料和娱乐融合在一个有趣的虚拟购物中心内，通过带有直播购物的大型数字化屏幕进行浏览和交易。它有一个无人送货系统，人们可以在那里购物和接收货物，享受快捷服务。
</div>
                    </div>
                </div>


                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-9 rep-image-section">
                        
                        <img id="rep-image-three" src="http://cdn.cn.neuronev.co/assets/images/rep/rep10.jpg" data-aos="fade-right" alt="hub_culture" />

                        </div>
                        <div className="col-sm-3 rep-content-section">
                            <div style= {{     fontSize: '20px',marginBottom: '10px'}}>自身学习充电</div>
<div style={{ fontSize: '12px'}}>REP同时也是一个体验中心，用于试驾和了解更多关于牛仁电动汽车的信息，目的是帮助人们更多地了解环保技术。</div>
                        </div>

                    </div>
                </div>
                <div className="blank-section-rep"></div>
                <div className="env-content">
                牛仁“REP”连接人、技术与环境
                </div>

                <div className="rep-image-one container-fluid rep-sec-bottom">
                    <img id="rep-last" src="http://cdn.cn.neuronev.co/assets/images/rep/rep11.jpg" alt="img-one" />
                </div>
                <Footer/>
             
             </main>
            </div>

        )
    }
}

export default RepSection