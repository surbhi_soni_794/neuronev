import React from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import {slidingImages, slidingImage} from '../../store/actions/index';
import $ from 'jquery';

let myIndex = 0;
let intervalVal ;
let valueData = false;
let inter;


class Met extends React.Component {


    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 1500;
            slidingImages(duration, fadeAmount, durationTime);
        })
    }

    componentDidMount() {
        document.title= "MET - Neuron EV";
        if(document.body.querySelector('.star-main-wrapper'))
        document.body.querySelector('.star-main-wrapper').addEventListener('scroll', this.aosInit);
        this.carousel();
    }
    
    aosInit(){
        window.AOS.init({
          duration: 2000,
        });
      }
    


    carousel = () => {
        var x = document.getElementsByClassName("slides-truck");
        let y = document.getElementsByClassName('mini-truck');
        if (x.length > 0 && y.length > 0) {
            var i = 0;
            for (i; i < x.length; i++) {
                x[i].style.display = "none";
                y[i].style.opacity = 0.5;
            }
            myIndex++;
            if (myIndex > x.length) { myIndex = 1 }
            x[myIndex - 1].style.display = "block";
            y[myIndex - 1].style.opacity = 1;

            intervalVal = setTimeout(this.carousel, 1500);
        }
    }

    clearData = () => {
        clearInterval(intervalVal);
        var x = document.getElementsByClassName("slides-truck");
        let y = document.getElementsByClassName('mini-truck');
        if (x.length > 0 ) {
            var i = 0;
            for (i; i < x.length; i++) {
                x[i].style.display = "none";
                y[i].style.opacity = 0.5;

            }
        }
        intervalVal = null;
        clearInterval(inter);

    }

    changeImage = (index) => {
        this.clearData()
        if (index && !valueData) {
            myIndex = 0;
            valueData = true
        }
        var x = document.getElementsByClassName("slides-truck");
        let y = document.getElementsByClassName('mini-truck');
        if (x.length > 0 && y.length > 0 ) {
            var i = index ? index : 0;
            for (i; i < x.length; i++) {
                x[i].style.display = "none";
                y[i].style.opacity = 0.5;
            }
            myIndex++;
            if (myIndex > x.length) { myIndex = 1 }
            x[index].style.display = "block";
            y[index].style.opacity = 1;

            inter = setTimeout(this.carousel, 1500);
        }
    }

    goToSection =() => {
        var elmnt = document.getElementById("star-section");
        elmnt.scrollIntoView();
      }

    render() {
        return (
            <div className="background-image">
                <div className="cursor-pointing">
                    <div className="dash-container" onClick={this.goToTop}><span className="dash-value first-val">1</span><span onMouseEnter={this.enterFirst} onMouseLeave={this.leaveFirst} className="map-dash"></span></div>
                    <div className="dash-container" onClick={this.goToBottom}><span className="dash-value second-val">2</span> <span onMouseEnter={this.enterSecond} onMouseLeave={this.leaveSecond} className="map-dash"></span></div>
                </div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <section className="truck-section-image met-image-wrapper met-sections">
                    <div className="multipurppose-text met-text">
                        {/* <img alt="img-truck" className="truck-img" src="http://cdn.cn.neuronev.co/assets/images/1.jpg" /> */}
                        <div className="multipurpose-txt-center">模块化中型电动汽车</div>
                        <div onClick={this.goToSection} className="arrow-image-my-own-img">
                            <div className="set-index-arrow -arrow-page met-arrow" id='met-arrow-page'>
                                <img id="drop-down-image" alt="down-img" src="http://cdn.cn.neuronev.co/assets/images/down-3.png" />
                            </div>
                        </div>
                    </div>
                </section>
                <div className="dash-section">
                    <div className="dash-section-ontainer">
                        <div className="numeric-star-data numeric-block-content">
                            1
                        </div>
                        <div className="dash-content-top"></div>
                    </div>
                    <div className="dash-section-ontainer">
                        <div className="numeric-star-data">
                            2
                         </div>
                        <div className="dash-content-top"></div>
                    </div>

                </div>
                <section className="met-parallax-img-wrapper parallax bg1">  </section>
                <div className="star-section" id='star-section'>
                    <div>MET</div>
                </div>
                <div className="container-fluid">
                <div className="map-video row vision-wrapper mt-60">
                    <iframe title="vision-video"
                            src="https://player.vimeo.com/video/364098005?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                            frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                    </div>
                </div>
                <div className="container-fluid star-culture-section">
                    <div className="row met-second-section">
                        <div className="col-sm-2 col-xs-12 col-12"></div>
                        <div className="col-lg-7 col-sm-7 col-xs-12 col-12">
                            <img src="http://cdn.cn.neuronev.co/assets/images/8.jpg" className="star-img-culture" alt="star_culture" />
                            <div className="met-border"></div>
                        </div>
                        <div className="col-lg-3 col-sm-3 col-xs-12 col-12">
                            <h1 className="star-culture-heading">中型电动汽车</h1>
                            <h3 className="neuronev-ev"> 牛仁“MET”是一款多功能汽车，具有广泛的商业应用前景。它被认为是未来商用卡车运输的简单解决方案。它在一个标准卡车结构上行驶，该结构经过改进以集成电动推进。平滑而强劲的转弯座舱优化了内部空间和空气动力流的后方架构。</h3>
                        </div>
                    </div>
                    {/* <div className="star-underline met-border"></div> */}

                </div>
                <div className="container-fluid star-versatile-container met-versatile-container">
                    <div className="star-versatile-section">
                        <h3 className="star-versatile-heading met-text-wrapper">
                        强大的多功能性
                    </h3>
                        <img src="http://cdn.cn.neuronev.co/assets/images/MicroT_ext39.png" data-aos="zoom-in-up" className="MicroT_ext39" id="zoom-in-val" alt="MicroT_ext39" />
                    </div>
                </div>
                <div className="container-fluid star-commercial-container met-commercial-wrapper">
                    <div className="star-versatile-section">
                        <h3 className="star-versatile-heading commercial-met">
                        电动商业运输
                    </h3>
                        <img src="http://cdn.cn.neuronev.co/assets/images/MicroT_ext38.png" data-aos="fade-right" id="zoom-in-val-2-met" className="versatile-image-section  MicroT_ext38" alt="MicroT_ext40" />
                    </div>
                </div>
                <div className="container-fluid micro-section met-micro-section">
                    <div className="row">
                        <div className="col-sm-6 pb-3">
                            <img id="left-image-slide" src="http://cdn.cn.neuronev.co/assets/images/5.jpg" data-aos="fade-right" className="MicroT_ext39 " alt="5.png" />
                        </div>
                        <div className="col-sm-6 ">
                            <img id="right-image-slide" src="http://cdn.cn.neuronev.co/assets/images/7.jpg" data-aos="fade-left" className="MicroT_ext39 " alt="MicroT_ext42" />
                        </div>

                    </div>
                </div>
                <div className="container-fluid micro-section bottom-container">
                    <div className="row">
                        <div className="col-12 col-sm-12">
                            <div className="image-container-star container-larger-img w3-content w3-section all" id="slideshow">
                                <img alt="imge-3" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/1.jpg" />
                                <img alt="imge-4" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/3.jpg" />
                                <img alt="img-5" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/4.jpg" />
                                <img alt="img-6" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/5.jpg" />
                                <img alt="img-7" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/6.jpg" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container-fluid star-commercial-container humanity-container">
                    <div className="start-versatile-blank-section"></div>
                    <div className="star-versatile-section">
                        <h3 className="star-versatile-heading commercial2">
                        丰富人们生活
                    </h3>
                        <h3 className="star-versatile-para">MET” 的后部是可互换的，适用于城市物流、卫生、建筑和采矿等多种功能领域当中。</h3>
                        <h3 className="star-versatile-para another-versatile-top">车辆的结构支撑刚性和铰接式卡车框架，为商业应用提供灵活的平台。</h3>

                        <div className="col-12 col-sm-12">
                            <div className="image-container-star container-larger-img w3-content w3-section all">
                                <img src="http://cdn.cn.neuronev.co/assets/images/MicroT_ext34.png" className="MicroT_ext34 slides-truck" alt="MicroT_ext39" />
                                <img  src="http://cdn.cn.neuronev.co/assets/images/MicroT_ext37.png" className="MicroT_ext37 slides-truck" alt="MicroT_ext40" />
                                <img  src="http://cdn.cn.neuronev.co/assets/images/MicroT_ext36.png" className="MicroT_ext36 slides-truck" alt="MicroT_ext41" />
                                <img  src="http://cdn.cn.neuronev.co/assets/images/met/met1.png" className="MicroT_ext40 slides-truck" alt="MicroT_ext40" />

                            </div>
                            <div className="container-fluid">
                                <div className="row star-microt-img">

                                    <div className="col-3 col-sm-3  mini-truck">
                                        <img  onClick={()=>this.changeImage(0)} src="http://cdn.cn.neuronev.co/assets/images/met/MicroT_ext34.png" className="MicroT_ext39 micot-img img-fluid" alt="MicroT_ext39" />
                                    </div>
                                    <div className="col-3 col-sm-3 mini-truck">
                                        <img onClick={()=>this.changeImage(1)} src="http://cdn.cn.neuronev.co/assets/images/met/MicroT_ext37.png" className="MicroT_ext40 micot-img img-fluid" alt="MicroT_ext37" />
                                    </div>
                                    <div className="col-3 col-sm-3 mini-truck">
                                        <img onClick={()=>this.changeImage(2)} src="http://cdn.cn.neuronev.co/assets/images/met/MicroT_ext36.png" className="MicroT_ext41 micot-img img-fluid" alt="MicroT_ext36" />
                                    </div>
                                    <div className="col-3 col-sm-3 mini-truck">
                                        <img onClick={()=>this.changeImage(3)} src="http://cdn.cn.neuronev.co/assets/images/met/MicroText45.png" className="MicroT_ext42 micot-img img-fluid" alt="MicroT_ext45" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
              
              </main>
            </div>
        );
    }
}
export default Met