import React from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import {slidingImages} from '../../store/actions/index';
import $ from 'jquery';


let myIndex = 0;
let intervalVal ;
let valueData = false;
let inter;

class Star extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isHoverTop: false,
            isHoverBottom: false
        }
        // $(document).ready(() => {
        //     let duration = 18;
        //     let fadeAmount = 0.3;
        //     let durationTime = 800;
        //     slidingImages(duration, fadeAmount, durationTime);
            
        // })
    }

    componentDidMount() {
        document.title= "STAR - Neuron EV";
        if(document.body.querySelector('.star-main-wrapper'))
        document.body.querySelector('.star-main-wrapper').addEventListener('scroll', this.aosInit);

        this.carousel()
    }
    aosInit(){
        window.AOS.init({
          duration: 2000,
        });
      }
    

    carousel = () => {
        var x = document.getElementsByClassName("slides-truck");
        let y = document.getElementsByClassName('mini-truck');
        let z = document.getElementsByClassName('image-animation-content');
        if (x.length > 0 && y.length > 0 && z.length > 0) {
            var i = 0;
            for (i; i < x.length; i++) {
                x[i].style.display = "none";
                y[i].style.opacity = 0.5;
                z[i].style.display = 'none';
            }
            myIndex++;
            if (myIndex > x.length) { myIndex = 1 }
            x[myIndex - 1].style.display = "block";
            y[myIndex - 1].style.opacity = 1;
            z[myIndex - 1].style.display = "block";

            intervalVal = setTimeout(this.carousel, 1500);
        }
    }

    clearData = () => {
        clearInterval(intervalVal);
        var x = document.getElementsByClassName("slides-truck");
        let y = document.getElementsByClassName('mini-truck');

        if (x.length > 0 ) {
            var i = 0;
            for (i; i < x.length; i++) {
                x[i].style.display = "none";
                y[i].style.opacity = 0.5;
            }
        }
        intervalVal = null;
        clearInterval(inter);

    }


    changeImage = (index) => {
        this.clearData()
        if (index && !valueData) {
            myIndex = 0;
            valueData = true
        }
        var x = document.getElementsByClassName("slides-truck");
        let y = document.getElementsByClassName('mini-truck');
        let z = document.getElementsByClassName('image-animation-content');
        if (x.length > 0 && y.length > 0 && z.length > 0) {
            var i = index ? index : 0;
            for (i; i < x.length; i++) {
                x[i].style.display = "none";
                y[i].style.opacity = 0.5;
                z[i].style.display = 'none';
            }
            myIndex++;
            if (myIndex > x.length) { myIndex = 1 }
            x[index].style.display = "block";
            y[index].style.opacity = 1;
            z[index].style.display = "block";

            inter = setTimeout(this.carousel, 1500);
        }
    }


    onMouseOverDashOne = (e) => {
        this.setState({ isHoverTop: true })
    }
    onMouseLeaveDashOne = (e) => {
        this.setState({ isHoverTop: false })
    }
    onMouseOverDashTwo = (e) => {
        this.setState({ isHoverBottom: true })
    }
    onMouseLeaveDashTwo = (e) => {
        this.setState({ isHoverBottom: false })
    }

    goToPageEight = (e) => {
        window.location.href = "#new-page-8";
        e.preventDefault();
        e.stopPropagation();
    }
    goToPageNine = (e) => {
        window.location.href = "#star";
        e.preventDefault();
        e.stopPropagation();
    }

    enterFirst = () => {

        if (document.getElementsByClassName('first-val').length > 0)
            document.getElementsByClassName('first-val')[0].style.visibility = 'initial';
    }
    enterSecond = () => {

        if (document.getElementsByClassName('second-val').length > 0)
            document.getElementsByClassName('second-val')[0].style.visibility = 'initial';

    }
    leaveFirst = () => {
        if (document.getElementsByClassName('first-val').length > 0)
            document.getElementsByClassName('first-val')[0].style.visibility = 'hidden';
    }

    leaveSecond = () => {
        if (document.getElementsByClassName('second-val').length > 0)
            document.getElementsByClassName('second-val')[0].style.visibility = 'hidden';
    }
    goToBottom = () => {
        var elmnt = document.getElementById("star");
        elmnt.scrollIntoView();
    }
    goToTop = () => {

        var elmnt = document.getElementById("new-page-8");
        elmnt.scrollIntoView();
    }
    goToNextSection = () => {
            var nextSection = document.getElementById("star");
            nextSection.scrollIntoView();
    }
    render() {
        // const { isHoverTop, isHoverBottom } = this.state;

        return (
            <div className="background-image">
                <div className="cursor-pointing">
                    <div className="dash-container" onClick={this.goToTop}><span className="dash-value first-val">1</span><span onMouseEnter={this.enterFirst} onMouseLeave={this.leaveFirst} className="map-dash"></span></div>
                    <div className="dash-container" onClick={this.goToBottom}><span className="dash-value second-val">2</span> <span onMouseEnter={this.enterSecond} onMouseLeave={this.leaveSecond} className="map-dash"></span></div>
                </div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />

                <section className="truck-section-image star-banner" id="new-page-8">
                    <div className="multipurppose-text star-text-section">
                        {/* <img alt="img-truck" className="truck-img" src="http://cdn.cn.neuronev.co/assets/images/2.jpg" /> */}
                        <div className="multipurpose-txt-center">电动多功能微型卡车</div>
                        <div onClick={this.goToNextSection} className="arrow-image-my-own-img">
                            <div className="set-index-arrow star-arrow-page" id="star-arrow-page">
                                <img id="drop-down-image" alt="down-img" src="http://cdn.cn.neuronev.co/assets/images/down-3.png" />
                            </div>
                        </div>
                    </div>
                </section>
                <section className="star-parallax-img-wrapper parallax bg1">  </section>
                <div className="star-section" id="star">
                    <div>STAR</div>
                </div>
                <div className="container-fluid">
                    {/* <video autoPlay loop className="star-video">
                        <source src="http://cdn.cn.neuronev.co/assets/images/Star_exterior_360.mp4" type="video/mp4" />
                    </video> */}
                    <div className="starvideo-wrapper">
                        <iframe title="video" src="https://player.vimeo.com/video/363938805?background=1&amp;autoplay=1&amp;loop=1&amp;title=0&amp;byline=0&amp;portrait=0" style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen="" id="yui_3_17_2_1_1575971377461_100"></iframe>
                    </div>
                </div>
                <div className="container-fluid star-culture-section">
                    <div className="row">
                        <div className="col-sm-2 col-xs-12 col-12"></div>
                        <div className="col-lg-8 col-sm-8 col-xs-12 col-12">
                            <img src="http://cdn.cn.neuronev.co/assets/images/star/7.jpg" className="star-img-culture" alt="star_culture" />
                            
                        </div>
                        <div className="col-lg-2 col-sm-2 col-xs-12 col-12" >
                            <h1 className="star-culture-heading">电动车文化新浪潮</h1>
                            <h3 className="neuronev-ev"> 牛仁电动车“STAR”这一款车型干净简约的设计体现了公司追求简单多功能的产品理念。这是一款城市环保卡车，散发着充满乐趣的个性，有些人戏称它为一艘带轮子的宇宙飞船。这种微型卡车有望在年轻一代中产生深刻的共鸣。</h3>
                        </div>
                    </div>
                    <div className="star-underline"></div>
                </div>
                <div className="container-fluid star-versatile-container">
                    <div className="star-versatile-section">
                        <h3 className="star-versatile-heading">
                            简约多功能
                        </h3>
                        <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext39.png" id="zoom-in-val" className="MicroT_ext39" alt="MicroT_ext39" data-aos="zoom-in-up" />
                    </div>
                </div>
                <div className="container-fluid star-commercial-container">
                    <div className="star-versatile-section">
                        <h3 className="star-versatile-heading commercial">
                            商用与私家合二为一
                        </h3>
                        <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext40.png" data-aos="fade-left" className="MicroT_ext39 star-Micro39" alt="MicroT_ext40" />
                    </div>
                </div>
                <div className="container-fluid micro-section star-both-images">
                    <div className="row">
                        <div className="col-sm-6 pb-4">
                            <img src="http://cdn.cn.neuronev.co/assets/images/star/5.jpg" data-aos="fade-right" id="left-image-slide" className="MicroT_ext39" alt="5.png" />
                        </div>
                        <div className="col-sm-6 ">
                            <img src="http://cdn.cn.neuronev.co/assets/images/star/4.jpg" data-aos="fade-left" id="right-image-slide" className="MicroT_ext39" alt="MicroT_ext42" />
                        </div>

                    </div>
                </div>
                <div className="container-fluid micro-section star-multiple-images">
                    <div className="row">
                        <div className="col-12 col-sm-12 images-bottom-slides">
                            <div className="image-container-star container-larger-img w3-content w3-section">
                                <img alt="imge-11" className="image-animation-content" src="http://cdn.cn.neuronev.co/assets/images/star/11.jpg" />
                                <img alt="imge-6" className="image-animation-content" src="http://cdn.cn.neuronev.co/assets/images/star/6.jpg" />
                                <img alt="img-10" className="image-animation-content" src="http://cdn.cn.neuronev.co/assets/images/star/10.jpg" />
                                <img alt="img-12" className="image-animation-content" src="http://cdn.cn.neuronev.co/assets/images/star/12.jpg" />
                                <img alt="img-9" className="image-animation-content" src="http://cdn.cn.neuronev.co/assets/images/star/9.jpg" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container-fluid star-commercial-container humanity-container">
                    {/* <div className="star-underline border-top-h3"></div> */}
                    <div className="start-versatile-blank-sectsion"></div>
                    <div className="star-versatile-section border-styling margin-bottom-star">

                        <h3 className="star-versatile-heading hr-wrapper ">

                            丰富人们生活
                        </h3>
                        <h3 className="star-versatile-para">牛仁电动汽车将电动汽车技术视为技术与自然的融合，以丰富人们的生活。</h3>

                        <div className="col-12 col-sm-12">
                            <div className="image-container-star container-larger-img w3-content w3-section all">
                                <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext43.png" className="MicroT_ext43 slides-truck" alt="MicroT_ext43" />
                                <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext42.png" className="MicroT_ext42 slides-truck" alt="MicroT_ext42" />
                                
                                <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext41.png" className="MicroT_ext41 slides-truck" alt="MicroT_ext41" />
                                
                                <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext40.png" className="MicroT_ext40 slides-truck" alt="MicroT_ext40" />
                                
                                <img src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext39.png" className="MicroT_ext39 slides-truck" alt="MicroT_ext39" />
                            
                            </div>
                            <div className="container-fluid">
                                <div className="row star-microt-img star-slides">
                                    <div className="col-2 col-sm-2  mini-truck">
                                        <img onClick={()=> this.changeImage(0)} src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext43.png" className="MicroT_ext39 micot-img img-fluid" alt="MicroT_ext39" />
                                    </div>
                                    <div className="col-2 col-sm-2 mini-truck">
                                        <img onClick={()=> this.changeImage(1)} src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext42.png" className="MicroT_ext40 micot-img img-fluid" alt="MicroT_ext40" />
                                    </div>
                                    <div className="col-2 col-sm-2 mini-truck">
                                        <img onClick={()=> this.changeImage(2)} src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext41.png" className="MicroT_ext41 micot-img img-fluid" alt="MicroT_ext41" />
                                    </div>
                                    <div className="col-2 col-sm-2 mini-truck">
                                        <img onClick={()=> this.changeImage(3)} src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext40.png" className="MicroT_ext42 micot-img img-fluid" alt="MicroT_ext42" />
                                    </div>
                                    <div className="col-2 col-sm-2 mini-truck">
                                        <img onClick={()=> this.changeImage(4)} src="http://cdn.cn.neuronev.co/assets/images/star/MicroT_ext39.png" className="MicroT_ext43 micot-img img-fluid" alt="MicroT_ext43" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <Footer/>
              
                </main>
            </div>
        )
    }
}

export default Star