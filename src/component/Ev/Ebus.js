import React, { Component } from 'react';
import { slidingImages } from '../../store/actions/index';
import Header from '../Header';
import Footer from '../Footer';

import $ from 'jquery';
import RespHeader from '../RespHeader';

export default class Ebus extends Component {
    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 1500;
            slidingImages(duration, fadeAmount, durationTime);

        })
    }

    componentDidMount() {
        document.title = "E BUS - Neuron EV";
        if(document.body.querySelector('.star-main-wrapper'))
        document.body.querySelector('.star-main-wrapper').addEventListener('scroll', this.aosInit);
    }
    aosInit(){
        window.AOS.init({
          duration: 2000,
        });
      }

    goToEbusSection = () => {
        var elmnt = document.getElementById("ebus-heading");
        elmnt.scrollIntoView();
    }

    goToBottom = () => {
        var elmnt = document.getElementById("ebus-heading");
        elmnt.scrollIntoView();
    }
    goToTop = () => {

        var elmnt = document.getElementById("top-id");
        elmnt.scrollIntoView();
    }

    enterFirst = () => {

        if (document.getElementsByClassName('first-val').length > 0)
            document.getElementsByClassName('first-val')[0].style.display = 'block';
    }
    enterSecond = () => {

        if (document.getElementsByClassName('second-val').length > 0)
            document.getElementsByClassName('second-val')[0].style.display = 'block';

    }
    leaveFirst = () => {
        if (document.getElementsByClassName('first-val').length > 0)
            document.getElementsByClassName('first-val')[0].style.display = 'none';
    }

    leaveSecond = () => {
        if (document.getElementsByClassName('second-val').length > 0)
            document.getElementsByClassName('second-val')[0].style.display = 'none';
    }


    render() {
        return (
            <React.Fragment>
                <div className="ebus-container">
                    <div className="cursor-pointing">
                        <div className="dash-container" onClick={this.goToTop}><span className="dash-value first-val">1</span><span onMouseEnter={this.enterFirst} onMouseLeave={this.leaveFirst} className="map-dash"></span></div>
                        <div className="dash-container" onClick={this.goToBottom}><span className="dash-value second-val">2</span> <span onMouseEnter={this.enterSecond} onMouseLeave={this.leaveSecond} className="map-dash"></span></div>
                    </div>
                    <RespHeader />
                    <main className="star-main-wrapper">
                        <Header />
                        <div className="ebus-first-section" id="top-id">
                            {/* <img alt="N1" className="ebus-img" src="http://cdn.cn.neuronev.co/assets/images/ebus/N1.jpg" /> */}
                            <h4 className="ebus-image-heading">模块化电动公共汽车</h4>
                            <div onClick={this.goToEbusSection} className="ebus-arrow-container">
                                <div className="arrow-image-container">
                                    <img className="aa" id="drop-down-image" alt="down-img" src="http://cdn.cn.neuronev.co/assets/images/down-3.png" />
                                </div>
                            </div>
                        </div>
                        <section className="ebus-parallax-img-wrapper parallax bg1">  </section>
                        <div className="ebus-heading" id="ebus-heading">
                            E-Bus
                </div>
                        <div className="container-fluid">
                            <div className="map-video row vision-wrapper mt-60">
                                <iframe title="vision-video"
                                    src="https://player.vimeo.com/video/354778686?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                                    frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                                </iframe>
                            </div>
                            </div>
                        <div className="ebus-second-section">
                            <div className="ebus-second-image-container">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/nbus8.jpg" data-aos="fade-right" alt="nbus8" />
                            </div>
                            <div className="section-second-heading-container">
                                <h1>
                                    世界上第一个多功能电动汽车品牌
                           </h1>
                                <h3>牛仁电动公共汽车搭建了一座桥梁，带我们最终走向清洁、绿色、智能的公共交通运输方式。
                           </h3>
                                <h3>电动公共汽车是完全模块化的，这意味着车辆的长度可以改变，以多种方式来提供服务，可采用不同交通运输形式，如小型公交车、公共巴士或铰接式客车，可满足社区和基础设施的多功能用途。</h3>
                                <h3>牛仁电动公共汽车可与高速铁路兼容，本质上是一种可以转换成快速地铁列车的客车。这项技术确实是汽车模块化多功能性方面的一项重大创新。</h3>
                            </div>
                        </div>
                        <div className="ebus-border-bottom"></div>
                        <div className="ebus-smart-powerfull">
                            <h1 className="smart-heading">
                                智能&强大
                    </h1>
                            <div className="image-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/bus99.png" data-aos="fade-up" data-aos-duration="3000" id="zoom-in-ebus-1" alt="buss99" />
                            </div>
                            <p> 牛仁的清洁能源公共汽车既可由驾驶员驾驶，也可以实现自动驾驶模式，其前部可以互换，以满足空气动力学要求。</p>
                            <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/busfront2copy.png" data-aos="fade-right" className="n4-image" id="zoom-in-ebus-2" alt="busfront2" />
                            <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/N4.png"  id="zoom-in-ebus-fadeIn" alt="N4png" className="n4-image" />
                        </div>
                        <div className="side-byside-images first-m">
                            <div className="side-byside-wrapper">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/N7.jpg" data-aos="fade-right" alt="N7png" id="ebus-left-image-slide" className="n7-image" />
                            </div>
                            <div className="side-byside-wrapper">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/N5.jpg" data-aos="fade-left" alt="N5png" id="ebus-right-image-slide" className="n5-image" />
                            </div>
                        </div>
                        <div className="ebus-border-bottom ebus-border-bottom-sidebyside"></div>
                        <div className="smart-section">
                            <h1 className='smart-heading'> 智能解决方案</h1>
                            <p className="smart-solution-para"> 牛仁以独特的视角通过运输促进人类发展。我们的多样性和前瞻性的汽车创新是为现实世界的应用而创造。牛仁可以改变整个生态系统。</p>
                            <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/b1copy.png"  alt="b1copy" className="b1copy-image"  data-aos="fade-left" />
                        </div>
                        <div className="neuron-section">
                            <div className="neuron-section-image-container">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/b1.png" data-aos="fade-right" id="zoom-in-ebus-4" alt="b1png" className="n7-image" />
                            </div>
                            <div className="neuron-section-content">
                                <h1>牛仁可以改变整个生态系统</h1>
                                <h3>尽管现在世界上大约17%的公共汽车是电动的，但这些绿色交通工具中99%是在中国。在这些公共汽车中，亚洲大国占了42.5万多辆，而美国仅略多于300辆。
                        </h3>
                                <h3>这一切都将在牛仁电动汽车进入市场后发生改变。随着越来越多的州和城市开始接受新技术，牛仁电动汽车发展将迎来一个好时机。加州是领头羊，其规定到2029年，该州公共交通机构购买的所有公共汽车都应为零排
                        </h3>
                            </div>
                        </div>
                        <div className="back-bus">
                            <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/back.png"  id="zoom-in-ebus-6" alt="backpng" className="b1copy-image" data-aos="fade-up" data-aos-duration="3000" />
                        </div>
                        <div className="future-section">
                            <h1 className='smart-heading'>我们共同的未来</h1>
                            <p className="smart-solution-para"> 牛仁电动汽车清楚地认识到电动商用车是未来的发展方向，凭借其优秀的人才和坚持不懈的研究，他们将走在这一新兴交通行业的前列。
                    </p>
                            <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/b_side.jpg" alt="b_sidejpg" id="zoom-in-ebus-7" className="b1copy-image" />
                        </div>
                        <div className="ebus-border-bottom ebus-border-bottom-sidebyside bottom-border"></div>

                        <div className="seating-chart-section">
                            <h1 className="smart-heading">
                                座位表
                    </h1>
                            <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/seatingchart.png" data-aos="zoom-in" id="zoom-in-ebus-8" alt="seatingchartpng" className="b1copy-image" />
                        </div>
                        <div className="versatile-section">
                            <h1 className='smart-heading'>多功能技术 </h1>
                            <p className="smart-solution-para">牛仁正在通过汽车技术、汽车生产和移动基础设施的创新来改变交通运输业。我们的使命是在人、技术和环境之间建立一个持久的协同作用。
                    </p>
                        </div>

                        <div className="side-byside-images">
                            <div className="images-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/8 (3).jpg"  data-aos="fade-right" id="ebus-col-1" alt="8 (3).jpg" className="versatile-images" />
                            </div>
                            <div className="images-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/untitled.122.jpg" data-aos="fade-left" id="ebus-col-2" alt="untitled122" className="versatile-images " />
                            </div>
                        </div>
                        <div className="side-byside-images four-images">
                            <div className="images-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/untitled.123.jpg" data-aos="fade-right" alt="untitled123" id="ebus-col-3"  className="versatile-images" />
                            </div>
                            <div className="images-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/untitled.124.jpg" data-aos="fade-left" alt="untitled124" id="ebus-col-4" className="versatile-images " />
                            </div>
                        </div>
                        <div className="side-byside-images">
                            <div className="images-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/untitled.129.jpg" data-aos="fade-right" alt="untitled129" id="ebus-col-5"  className="versatile-images " />
                            </div>
                            <div className="images-block">
                                <img id="newly-id" src="http://cdn.cn.neuronev.co/assets/images/ebus/untitled.128.jpg" data-aos="fade-left" alt="Nuntitled128jpg" id="ebus-col-6" className="versatile-images " />
                            </div>
                        </div>
                        <div className="ebus-border-bottom ebus-border-bottom-sidebyside last-ebus-border"></div>
                       
                        <div className="container-fluid bottom-container-ebus">
                        <div className="row">
                            <div className="col-12 col-sm-12">
                                <div className="image-container-star container-larger-img w3-content w3-section all" id="slideshow">
                                    <img alt="imge-3" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/ebus/ebus1.jpg" />
                                    <img alt="imge-4" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/ebus/ebus2.jpg" />
                                    <img alt="img-5" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/ebus/ebus3.jpg" />
                                    <img alt="img-6" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/ebus/ebus4.jpg" />
                                    <img alt="img-7" className="image-animation-content-slide" src="http://cdn.cn.neuronev.co/assets/images/ebus/ebus5.jpg" />
                                </div>
                            </div>
                        </div>
                    </div>

                    
                     {/*  <div id="slideshow" className="ebus-last-section">
                            <div>
                            <img alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/ebus/N2.jpg" />
                            <img alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/ebus/N3.jpg" />
                            <img alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/ebus/N4.jpg" />
                            <img alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/ebus/N6.jpg" />
                            <img src="http://cdn.cn.neuronev.co/assets/images/ebus/N8.jpg" alt="N8jpg" />
                            <img src="http://cdn.cn.neuronev.co/assets/images/ebus/N7.jpg" alt="N7jpg" />
                            </div>
                        </div>
        */}

                        <Footer />

                    </main>
                </div>
            </React.Fragment>
        )
    }
}

