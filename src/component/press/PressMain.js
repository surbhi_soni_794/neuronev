import React from 'react';
import { PressContent } from './PressContent';
import { pressComman } from '../../store/actions';
import { connect } from 'react-redux';
import { actionTypes } from '../../store/types';
import ImagePreview from './ImagePreview';

class PressMain extends React.Component {
    state = {
        isPreview: false,
        srcPic: '',
    }

    componentDidMount() {
        const componentArr = PressContent;
        const routeName = window.location.pathname;

        componentArr.map(val => {
            if (routeName === '/121019') {
                pressComman(actionTypes.pressOne, val.pressOne)
            }
            else if (routeName === '/12-06-19') {
                pressComman(actionTypes.pressTwo, val.pressTwo)
            }
            else if (routeName === '/questionsandanswers') {
                pressComman(actionTypes.pressThree, val.pressThree)
            }
            else if (routeName === '/ciie-2019') {
                pressComman(actionTypes.ciie2019, val.ciie)
            }
            else if (routeName === '/evintro') {
                pressComman(actionTypes.evintro, val.evintro)
            }
            else if (routeName === '/expo3') {
                pressComman(actionTypes.expo3, val.expo3)
            }
            else if (routeName === '/expo2') {
                pressComman(actionTypes.expo2, val.expo2)
            }
            else if (routeName === '/starpress') {
                pressComman(actionTypes.starpress, val.starpress)
            }
            else if (routeName === '/metpress') {
                pressComman(actionTypes.metpress, val.metpress)
            }
            else if (routeName === '/vision-press') {
                pressComman(actionTypes.visionppress, val.visionpress)
            }
            else if (routeName === '/new-index-2') {
                pressComman(actionTypes.newindex2, val.newIndex2)
            }
            else if (routeName === '/ebus_press') {
                pressComman(actionTypes.ebus_press, val.ebus_press)
            }
            else if (routeName === '/mappress') {
                pressComman(actionTypes.mappress, val.mappress)
            }
            else if (routeName === '/hubpress') {
                pressComman(actionTypes.hubpress, val.hubpress)
            }
        })
    }

    goToNextSection = () => {
        var nextSection = document.getElementById("press-next");
        nextSection.scrollIntoView();
    }

    goToBottom = () => {
        var elmnt = document.getElementById("press-next");
        elmnt.scrollIntoView();
    }
    goToTop = () => {
        var elmnt = document.getElementById("top-id");
        elmnt.scrollIntoView();
    }

    enterFirst = () => {
        if (document.getElementsByClassName('first-val').length > 0)
            document.getElementsByClassName('first-val')[0].style.visibility = 'initial';
    }
    enterSecond = () => {
        if (document.getElementsByClassName('second-val').length > 0)
            document.getElementsByClassName('second-val')[0].style.visibility = 'initial';

    }
    leaveFirst = () => {
        if (document.getElementsByClassName('first-val').length > 0)
            document.getElementsByClassName('first-val')[0].style.visibility = 'hidden';
    }

    leaveSecond = () => {
        if (document.getElementsByClassName('second-val').length > 0)
            document.getElementsByClassName('second-val')[0].style.visibility = 'hidden';
    }

    onImagePreview = (src, id) => {
        this.setState({ isPreview: true, srcPic: id });
    }

    closeImagePreview = () => {
        this.setState({ isPreview: false, srcPic: '' });
    }
    render() {
        const { topBanner = '', topContent = {}, imagesArr = [], imagesProp = {}, dashContent = {}, topLogo = '' } = this.props.press;
        const { heading = '', heading2 = '', content = '' } = topContent;

        return (
            <div className="press-one-container container-width-press">
             
                {this.state.isPreview ?
                <ImagePreview closeImagePreview={this.closeImagePreview} srcPic={this.state.srcPic} imagLinks= {imagesArr}/> 
                 : null}
        
                <div className="cursor-pointing">
                    <div className="dash-container" onClick={this.goToTop}><span className="dash-value first-val">{dashContent.first}</span><span onMouseEnter={this.enterFirst} onMouseLeave={this.leaveFirst} className="map-dash"></span></div>
                    <div className="dash-container" onClick={this.goToBottom}><span className="dash-value second-val">{dashContent.second}</span> <span onMouseEnter={this.enterSecond} onMouseLeave={this.leaveSecond} className="map-dash"></span></div>
                </div>
                {topBanner && topBanner.length > 0 ?
                    <div style={{ backgroundImage: `url(${topBanner})` }} className="ebus-first-section press-main-conatiner" id="top-id">
                        <div onClick={this.goToNextSection} className="ebus-arrow-container">
                            <div className="arrow-image-container">
                                <img id="drop-down-image" alt="down-img" src="http://cdn.cn.neuronev.co/assets/images/down-3.png" />
                            </div>
                        </div>
                    </div>
                    :
                    <div className="vision-video-background press-main-conatiner video-container-top video-all-wrap" id="top-id">
                        {/* <div className="starvideo-wrapper vision-video"> */}
                            {/* <iframe title="video" src="https://player.vimeo.com/video/361236334?background=1&amp;autoplay=1&amp;loop=1&amp;title=0&amp;byline=0&amp;portrait=0" style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen="" id="yui_3_17_2_1_1575971377461_100"></iframe> */}
                        <video className="vision-video-all" muted autoPlay  loop >
                            <source src="http://cdn.cn.neuronev.co/assets/images/visionpress/1482062691.mp4" type="video/mp4"/>
                            </video> 
                        {/* </div> */}
                        <div onClick={this.goToNextSection} className="ebus-arrow-container">
                            <div className="arrow-image-container">
                                <img id="drop-down-image" alt="down-img" src="http://cdn.cn.neuronev.co/assets/images/down-3.png" />
                            </div>
                        </div>
                    </div>
                }

                <div className="pressone-heading" id="press-next">
                    {
                        topLogo && topLogo.length > 0 ? <div className={window.location.pathname ==='/mappress' ? "top-logo top-logo-map": "top-logo"}>
                            <img src={topLogo} alt="press-top-logo" />
                        </div> : null
                    }
                    <h2>{heading && heading}</h2>
                    {heading2 ? <h4>{heading2}</h4> : null}
                    <div className="press-contents">
                        <p>
                            {content.firstPar && content.firstPar}
                        </p>
                        <p>
                            {content.secondPar && content.secondPar}
                        </p>
                    </div>
                </div>
                <div className="press-images-section">
                    <ul className="press-images">
                        {imagesArr && imagesArr.length > 0 && imagesArr.map((img, idx) => (
                            <li className={window.location.pathname === '/evintro' ? `press-images-wrapper press-images-envintro` : `press-images-wrapper`} key={idx}>
                                <img onClick={() => this.onImagePreview(img, idx)} id={idx} alt="imgs" src={img} className="press-oneImg" style={{ height: `${imagesProp.height}` }} />
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        press: state.press.pressData
    }
}
export default connect(mapStateToProps)(PressMain);