import React from 'react';

var slideIndex = 1;

class ImagePreview extends React.Component {

    constructor() {
        super();
    }

    componentDidMount() {
        var slides = document.getElementsByClassName("mySlides-popup");
        if(slides.length > 0)
        slides[this.props.srcPic].style.display = "block";

    }


    plusSlides = (n) => {
        this.showSlides(slideIndex += n);
    }

    currentSlide = (n) => {
        this.showSlides(slideIndex = n);
    }

    showSlides = (n) => {
        var i;
        var slides = document.getElementsByClassName("mySlides-popup");
        if (slides.length > 0) {
            if (n > slides.length) { slideIndex = 1 }
            if (n < 1) { slideIndex = slides.length }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }

            slides[slideIndex - 1].style.display = "block";
        }
    }

    render() {
        return (
            <div className="image-preview-conatiner">
                <div className="prev-container">
                    <div onClick={this.props.closeImagePreview} className="popup-close"><i className="fa fa-close" /></div>
                    <div className="slideshow-container">
                    {this.props.imagLinks && this.props.imagLinks.length > 0 ?
                            this.props.imagLinks.map((imgs, idx) =>(
                                <div className="mySlides-popup fading" key={idx}>
                                    <img src={imgs}  key={idx} />
                                   
                                </div>
                                )
                            )
                            : null}
                        <span className="prev" onClick={() => this.plusSlides(-1)}>&#10094;</span>
                        <span className="next" onClick={()=>this.plusSlides(1)}>&#10095;</span>

                    </div>
                </div>
            </div>
        )
    }
}

export default ImagePreview

