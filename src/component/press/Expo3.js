import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Expo3 extends React.Component {
  componentDidMount() {
    document.title = "EXPO3 - Neuron EV";
  }

  render() {
    return (
      <>
      <div><RespHeader /></div>
      <Header />
        <PressMain />
        <div className="press-one-container press-page">
          <p>当他们说需求是发明之母时，是真理。现在，不可否认的是，电动汽车时代已来，并在此驻足。电动汽车、公共汽车和卡车主导着世界并不是一个是否的问题，而是一个何时的问题。 </p>
          <p>牛仁电动汽车将展示他们为促进电动汽车价值的愿景。该公司表明，他们的使命是创造真正需要的具有目的性汽车。此外，牛仁电动汽车采用了可再生交通的整体方法，集成了易于内外互换的组件。</p>
          <p>最近，“STAR”这款展示出牛仁简约多功能的特性。这是一款带有明确目的性的多功能用车的典型例子。这是一款环保，实用，具有永恒的性的车型。</p>
          <p>接下来是牛仁电动汽车在11月的中国国际进口展览会上首次展出的电动多功能用车和电动半挂车。</p>
          <p>多功能用车一直是消费者的最爱，他们需要的不仅仅是能从A点到B点的基本车型，他们真正需要的是一辆能够承载负载、应对恶劣天气和应对挑战性地形的多功能用车。</p>
          <p>几年前，节约环境是转向清洁能源运输的重要价值主张。现在，电动汽车在道路性能和性价比方面也有其优势。</p>
          <p>牛仁电动汽车采用一系列简单和多功能用车的车辆来升级其价值主张，所有车辆都具有多功能性。这推动了它们在个人和商业运输中的市场相关性。</p>
          <p>公司正期待着与客户建立联系，他们表示，正是这种联系确定了我们的愿景与客户需求是一致的。牛仁电动汽车即将交付的产品将给汽车行业带来革命性的变化。</p>
          <p>获取更多详情，请联系  <a href="mailto:pr@neuronev.co">pr@neuronev.co </a>  </p>
          <h5>公关新闻专线来源 </h5>
          <a href="https://www.prnewswire.com/news-releases/neuron-ev-ready-to-electrify-upcoming-ciie-300941748.html">https://www.prnewswire.com/news-releases/neuron-ev-ready-to-electrify-upcoming-ciie-300941748.html</a>
          <h5>相关链接 </h5>
        <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
      </div>
<Footer/>     
</>
    )
  }
}

export default Expo3