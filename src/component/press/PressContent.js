export const PressContent = [{
    pressOne: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1576110229222-NT24TVPMGLZPU5HGJ39A/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/Neuron_EV___A.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车在好莱坞和上海开幕',
            content: {
                firstPar: '加州尔湾2019年12月10日 /美通社/ -- 富有远见的牛仁电动汽车公司(Neuron EV) 正在筹备其备受期待的视觉分享展，该展览将在洛杉矶西好莱坞的现代高级社区开幕。',
                secondPar: '本次展览以“推动变革”为主题，将独家展示牛仁通过电动汽车来促进人类发展的非凡使命。通过展览，人们可以了解公司的历史、当前的发展和未来的展望。'
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',

        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/121019/Neuron_EV___A.jpg',
            'http://cdn.cn.neuronev.co/assets/images/121019/Neuron_EV___B.jpg',
            'http://cdn.cn.neuronev.co/assets/images/121019/Neuron_EV____C.jpg',
        ],
        imagesProp: {
            width: '414px',
            height: '260px'
        },
        dashContent: {
            first: '12/10/19_1',
            second: '12/10/19_2'
        }
    },
    pressTwo: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1575687994697-21CU44D3JW7RXS1J5795/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/Torq_Interior1.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车引入纯电动汽车TORQ',
            content: {
                firstPar: '加州尔湾2019年12月10日 /美通社/ -- 富有远见的牛仁电动汽车公司(Neuron EV)最近推出了牛仁电动汽车TORQ，该公司的电动半挂车为清洁能源汽车设定了新的标准。',
                secondPar: '可持续性'
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/12-06-19/Torq_Interior4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/12-06-19/Torq_Interior3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/12-06-19/Torq_Interior2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/12-06-19/Torq_Interior1.jpg',
            'http://cdn.cn.neuronev.co/assets/images/12-06-19/NEURON_TORQ_EXTERIOR_2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/12-06-19/NEURON_TORQ_EXTERIOR_1.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '344px'
        },
        dashContent: {
            first: 'TORQ_Press',
            second: 'TORQ_Press2'
        }
    },
    pressThree: {
        topBanner:'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1574449827685-16116LJ8ZFKASVXDDKHD/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/T_ONE_Exterior3.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车发布令人震撼的T.ONE 图片集和相关问答',
            content: {
                firstPar: '加州欧文，2019年11月22日/公关新闻专线/--牛仁电动汽车回答有关创新汽车公司在扩展电动汽车领域的历史、愿景、品牌本质、前景和作用的问题。',
                secondPar: '牛仁电动汽车到底是什么？'
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',

        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/t1.jpeg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Exterior2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Exterior3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Exterior4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Exterior5.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Exterior6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Interior1.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Interior2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Interior3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Interior4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Interior5.jpg',
            'http://cdn.cn.neuronev.co/assets/images/questionsandanswers/T_ONE_Interior6.jpg',
        ],
        imagesProp: {
            width: '414px',
            height: '258px'
        },
        dashContent: {
            first: 'Q&amp;A_1',
            second: 'Q&amp;A_2'
        }
    },
    ciie: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1574287831876-E6IZM671VUTBJAPK6LH0/ke17ZwdGBToddI8pDm48kCRzb3JqnyRQ7VUq2pE7P097gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0m_nwPNs2kYVhoOQZiVFxSZT2Wr24SOJoV2jGZLeubaMO4PtbSibPzliYe0F9dY0hQ/A.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车成为中国国际进口博览会焦点',
            content: {
                firstPar: '加州欧文，2019年11月20日/公关新闻专线/--牛仁电动汽车公司是世界上第一家电动多功能汽车公司，刚刚成功在中国上海第二届中国国际进口博览会完成了展览。',
                secondPar: '在福特和特斯拉（Tesla）展台旁的黑色和银色展台上，参展商们急切地想一睹这家卓有远见的汽车公司推出的新产品。'
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',

        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/ciie/A.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/B.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/D.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/C.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/9.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/8.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/7.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/5.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/neuronciie1.jpeg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/neuronciie2.jpeg',
            'http://cdn.cn.neuronev.co/assets/images/ciie/neuronciie3.jpeg',
        ],
        imagesProp: {
            width: '552px',
            height: '258px'
        },
        dashContent: {
            first: 'CIIE_1',
            second: 'CIIE_2'
        }
    },
    evintro: {
        topBanner:"https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1573089558051-34FE40L797X0350KTRWN/ke17ZwdGBToddI8pDm48kDP7kJAFAah05H3ChyNlHSF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0mxU0godxi02JM9uVemPLqy9uOsj-q5bCkZmGZoQ7_oEh9b9-wZU0EZkSsdXFC8ujw/1.jpg?format=1500w",
        topContent: {
            heading: '牛仁升级了中国国际进出口展览会电动汽车的新未来',
            content: {
                firstPar: '加州欧文，2019年11月6日/公关新闻专线/——富有远见的汽车公司牛仁电动汽车在中国上海第二届年度进口博览会上推出了两款令人兴奋的新产品：T/ONE和TORQ。',
                secondPar: '牛仁电动汽车在知名汽车制造商中独一无二，因为他们是第一家专注于可持续发展的电动多功能汽车公司和一个新品牌。'
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',

        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/evintro/1.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/7.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/9.jpg',
            'http://cdn.cn.neuronev.co/assets/images/evintro/8.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '230px'
        },
        dashContent: {
            first: '1',
            second: '2'
        }
    },
    expo3: {
        topBanner:'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1571700046443-U7N61N6FC5K4M6W2ZAU9/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/2_re.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车整装待发迎接中国国际进口博览会',
            content: {
                firstPar:'加州欧文，2019年10月21日/公关新闻专线——富有有远见的牛仁电动汽车将于今年11月发布产品预览。?',
                secondPar: '需要乃发明之母，这是一条真理。'
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',


        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/expo3/1_re.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo3/2_re.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo3/3_re.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo3/4_re.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo3/5_re.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo3/6.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: 'expo3_1',
            second: 'expo3_2'
        }
    },
    expo2: {
        topBanner:'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1570492017216-DAVZHMX4DR6W6E6Z648C/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/1.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动车为第二届年度中国国际进口博览会展览会发布预告片',
            content: {
                firstPar:'加州欧文，2019年10月7日/公关新闻专线/——富有有远见的牛仁电动汽车公司宣布，公司两款新产品将于今年11月在中国上海举行的第二届国际进口博览会上首次亮相。',
                secondPar: ''
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/expo2/1.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo2/2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/expo2/3.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: 'expo2_1',
            second: 'expo2_2'
        }
    },
    starpress: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1570120894329-1W4SG097BO37ACRMH0Y8/ke17ZwdGBToddI8pDm48kCX-V5vw-8h9IBXN10-_8XN7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0p4Wyba38KfG317vYluk45_zZdtnDCZTLKcP2mivxmYi50xvY5saIGKMgOza9mH4XA/2.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车推出模块化电动卡车',
            content: {
                firstPar:'IRVINE, Calif., Oct. 1, 2019 /PRNewswire/ -- 富有远见的牛仁电动汽车推出了一套新的电动多功能汽车，具有改变汽车行业的潜力。',
                secondPar: '公司非常荣幸地推出了牛仁电动汽车“MET”或中型电动卡车，以及牛仁电动汽车STAR，一款多功能微型卡车。这两款车都是电动敞篷卡车，后部可互换，以优化多功能性。',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/starpress/11.jpg',
            'http://cdn.cn.neuronev.co/assets/images/starpress/10.jpg',
            'http://cdn.cn.neuronev.co/assets/images/starpress/5.jpg',
            'http://cdn.cn.neuronev.co/assets/images/starpress/9.jpg',
            'http://cdn.cn.neuronev.co/assets/images/starpress/4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/starpress/12.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: '1',
            second: '2'
        }
    },
    metpress: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1570120567637-5FK9IQTDOCTKDWRCRTUZ/ke17ZwdGBToddI8pDm48kHH9S2ID7_bpupQnTdrPcoF7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0nQwvinDXPV4EYh2MRzm-RRB5rUELEv7EY2n0AZOrEupxpSyqbqKSgmzcCPWV5WMiQ/1.jpg?format=1500w',
        topContent: {
            heading: '牛仁电动汽车推出模块化电动卡车',
            content: {
                firstPar:'IRVINE, Calif., Oct. 1, 2019 /PRNewswire/ -- 富有远见的牛仁电动汽车推出了一套新的电动多功能汽车，具有改变汽车行业的潜力。',
                secondPar: '公司非常荣幸地推出了牛仁电动汽车“MET”或中型电动卡车，以及牛仁电动汽车STAR，一款多功能微型卡车。这两款车都是电动敞篷卡车，后部可互换，以优化多功能性。',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/metpress/2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/metpress/6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/metpress/9.jpg',
            'http://cdn.cn.neuronev.co/assets/images/metpress/7.jpg',
            'http://cdn.cn.neuronev.co/assets/images/metpress/4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/metpress/12b.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: '1',
            second: '2'
        }
    },
    visionpress: {
        topBanner: '',
        topContent: {
            heading: '牛仁电动汽车宣布独家预览产品机会',
            heading2: '创新的电动汽车公司为电动汽车的未来奠定基础 ',
            content: {
                firstPar:'加州欧文，2019年9月19日/公关新闻专线/——牛仁电动汽车宣布将在11月即将举行的国际活动上展示其产品，届时VIP可拥有独家预览牛仁电动汽车产品的机会。他们还计划向公众推广展览活动。',
                secondPar: '随着消费者开始将目光转向电动汽车，交通运输业目前处于行业的十字路口。',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/visionpress/DRIVE_NEURON.jpg',
            'http://cdn.cn.neuronev.co/assets/images/visionpress/1resized.jpg',
            'http://cdn.cn.neuronev.co/assets/images/visionpress/pickup_1.1.2.jpg',
        ],
        imagesProp: {
            width: '672px',
            height: '306px'
        },
        dashContent: {
            first: 'Vision_press1',
            second: 'Vision_press2'
        }
    },
    newIndex2: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1569539978621-72AJKJL21SLIBANO16CE/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/5.jpg?format=1500w',
         topContent: {
            heading: '牛仁电动汽车推动可再生能源交通领域',
            content: {
                firstPar:'加州欧文，2019年8月27日/公关新闻专线/——富有远见的牛仁电动汽车公司将对人类和环境带来重大变革。',
                secondPar: '公司正在创新驾驶体验，一种无噪音和环保的大众交通方式，旨在促进社区和整个交通行业的发展。这是公司将人、技术和环境联系起来一个长久承诺。',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/newIndex2/1.jpg',
            'http://cdn.cn.neuronev.co/assets/images/newIndex2/4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/newIndex2/3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/newIndex2/6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/newIndex2/7.jpg',
            'http://cdn.cn.neuronev.co/assets/images/newIndex2/8.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: 'NEW PAGE',
            second: 'NEW PAGE'
        }
    },
    ebus_press: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1569647866628-I8G0PXUH9RW4X3LIGX2F/ke17ZwdGBToddI8pDm48kFyD7pzB8zoMIVY5aiUuFlp7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0jG2lbcDYBOeMi4OFSYem8DMb5PTLoEDdB05UqhYu-xbnSznFxIRsaAU-3g5IaylIg/N5.jpg?format=1500w',
        topContent: {
            heading: 'NEURON EV STEERS INTO THE FUTURE WITH NEW ELECTRIC BUS',
            content: {
                firstPar:'IRVINE, Calif., Aug. 8, 2019 /PRNewswire/ -- Visionary automotive company Neuron EV has effectively ushered in the future of public transportation with the release of their new electric bus.',
                secondPar: 'The company has been notable for recently unveiling their latest technologies: the Multipurpose Autonomous Platform or MAP, a self-motorized platform that mounts structures of various sizes for placement and mobile residence, and Harmony-Utility-Balance or HUB, its intuitive on-demand transportation system that\'s been hailed as the future of ride-sharing.',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/ebuspress/N3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ebuspress/N1.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ebuspress/N6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ebuspress/N7.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ebuspress/N5.jpg',
            'http://cdn.cn.neuronev.co/assets/images/ebuspress/N2.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: 'NEW PAGE',
            second: 'NEW PAGE'
        }
    },
    mappress: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1569627770605-7CXOU8NHHBZOJ9F6UIS9/ke17ZwdGBToddI8pDm48kJ7qCJnd-9dUFi-EBo_swvp7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0q7uY_gEAXjzYg1fYWS2heTR4zYbwVz5EKjjXRsz4jBP22ICoanGH9vXH4t3q_5LvQ/1B.jpg?format=1500w',
         topContent: {
            heading: '牛仁电动汽车开启智能出行和居住的未来',
            content: {
                firstPar:'加州欧文，2019年7月8日/公关新闻专线/——富有远见的牛仁电动汽车最近发布了其最新技术，这项技术将彻底改变交通和房地产业。',
                secondPar: '多功能型自动平台或 “MAP”，一种安装各种尺寸结构的自动机动化平台，以便将它们从一个位置运输到另一个位置。它的大小可以根据具体的工作进行相应调整，例如可作为乘客的交通工具、货物的运输工具或旅行者的临时住所等等。',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/mappress/1B.jpg',
            'http://cdn.cn.neuronev.co/assets/images/mappress/6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/mappress/4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/mappress/7.jpg',
            'http://cdn.cn.neuronev.co/assets/images/mappress/3.jpg',
            'http://cdn.cn.neuronev.co/assets/images/mappress/5.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: 'map_press1',
            second: 'map_press2'
        }
    },
    hubpress: {
        topBanner: 'https://images.squarespace-cdn.com/content/v1/5d51e5f5c1ab400001473954/1569625029537-6HRQZ4NTRLXAOVXM0SKR/ke17ZwdGBToddI8pDm48kFWbfvKoGaFShhMcKfsq37t7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0ouw-8l5B_J38LMU7OZFvYcziOCI9cMyju6s5Dts8o5YzjUQu2gQZ2SBUKJEz7xDLA/NeuronEV_3.jpg?format=1500w',
         topContent: {
            heading: '牛仁电动汽车 “HUB”——交通新模式',
            content: {
                firstPar:'加州欧文，2019年5月10日/公关新闻专线/——今天，富有远见的牛仁电动车向公众展示了HUB——一款代表智能交通的未来的车型。HUB代表和谐、多功能、平衡。HUB是技术的融合，易于出行。枢纽的愿景是统一而与众不同，多样而不分散。HUB是一种新的交通运输模式，旨在对我们的社会产生积极的影响。 ',
                secondPar: '',
            }
        },
        topLogo: 'http://cdn.cn.neuronev.co/assets/images/starpress/NEURON_LOGO-01.png',
        imagesArr: [
            'http://cdn.cn.neuronev.co/assets/images/hubpress/NeuronEV_4.jpg',
            'http://cdn.cn.neuronev.co/assets/images/hubpress/NeuronEV_5.jpg',
            'http://cdn.cn.neuronev.co/assets/images/hubpress/NeuronEV_2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/hubpress/n_park2.jpg',
            'http://cdn.cn.neuronev.co/assets/images/hubpress/NeuronEV_6.jpg',
            'http://cdn.cn.neuronev.co/assets/images/hubpress/NeuronEV_1.jpg',
        ],
        imagesProp: {
            width: '552px',
            height: '306px'
        },
        dashContent: {
            first: 'hub_press1',
            second: 'hub_press2'
        }
    },
}]




