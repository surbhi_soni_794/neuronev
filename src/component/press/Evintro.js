import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Evintro extends React.Component {

  componentDidMount() {
    document.title = "EV INTRO - Neuron EV";
  }

  render() {
    return (
      <>
      <div><RespHeader /></div>
      <Header />
        <PressMain />
        <div className="press-one-container press-page">
          <p>牛仁T/ONE是公司创新的多功能一体式EUV（电动多功能车），用于商业和个人用途，而牛仁 TORQ是一款电动半挂车，致力于开发商业运输的下一代解决方案。</p>
          <p>两款车均采用模块化平台、可升级技术、多功能货物功能、一流的内饰体验和标志性的空气动力学。这些车辆是为了减少浪费，并为保护环境提出可再生的解决方案。</p>
          <p>T/ONE和TORQ是建立在牛仁的模块化电动车系统上的。基本的拖拉机单元有一个可伸缩的底盘，带有可互换的车身部件，易于连接和分离，便于改变其外观和功能。</p>
          <p>光滑的电动传动系统优化了地板空间，降低了重心，创造了一个易于驾驶的平台。机舱前移，以提高驾驶能见度，优化内部空间，并扩大货物容量。 </p>
          <p>卡车的多源推进系统从全电力牵引电池组、可更换备用电源和可拆卸太阳能电池板卡车床罩中获取电力。这种设计符合牛仁对可再生能源的实际解决方案——适应自然资源的可用性，并朝着按需充电基础设施的方向创新。 </p>
          <p>由于无线摄像头可以将视觉数据输入驾驶员屏幕，因此驾驶员座椅具有全景360°视图。直观的车辆接口应用程序提供全面的操作控制，可用在任何个人设备上，插入内部架构后，可无缝取代所有物理旋钮和开关。这个无杂物的开放系统可兼容最新的小工具和互联网解决方案。 </p>
          <p>这两款卡车都体现了牛仁的产品理念，即通过简约性和功能性实现价值。牛仁品牌专家Casey Hyun说，牛仁易于使用，通用性强，可更新至最新技术，有效减少浪费和优化多功能性。</p>
          <p>卡车的功能结构是一个气动外形，具有后车厢集成功能。光滑而坚固的外壳保护了存储在车内的技术以及乘客在车内的安全。 </p>
          <p>牛仁的愿景是通过新能源运输的创新来丰富人类的生活。该公司计划通过打造连接人、技术和环境的车辆来推动电动汽车的发展。更具体地说，牛仁正在开发能够显著减少污染的智能车辆，同时满足消费者对实现连接和统一的车辆需求。</p>
          <p>牛仁电动汽车首席执行官爱德华•李（Edward Lee）表示，我们有一个理想，叫做“推动变革”，目的在提升未来可持续性方面实现统一。牛仁很荣幸能参加今年的中国国际进口博览会，我们期待与更多志同道合的专业人士交流，在全球范围内推动我们的目标。</p>
          <p>牛仁电动车是一家成立于2017年的加州电动汽车公司。</p>
          <p>
            获取更多详情，请联系 <a href="mailto:pr@neuronev.co">pr@neuronev.co </a> </p>
          <h5>公关新闻专线 </h5>
          <a href="https://www.prnewswire.com/news-releases/neuron-upgrades-the-future-of-ev-at-ciie-300953014.html?fbclid=IwAR1mHs93PJYHJSmQCkJNCjJoGUU59LbTMsJzwfOr2YfVUYYk-5ZxDrMHBWU">https://www.prnewswire.com/news-releases/neuron-upgrades-the-future-of-ev-at-ciie</a>
          <h5>相关链接 </h5>
          <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a>
        </div>
        <Footer/>
      </>
    )
  }
}

export default Evintro