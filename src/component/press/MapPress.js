import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class MapPress extends React.Component {
  componentDidMount() {
    document.title = "Map Press - Neuron EV";
  }

  render() {
    return (
      <>
      <div><RespHeader /></div>
      <Header />
        <PressMain />
        <div className="press-one-container press-page">
<p>MAP是一个带有15英尺的箱式房子，长6,670毫米，宽2,600毫米，高3,540毫米。</p>
<p>5月10日，公司宣布，MAP是牛仁“HUB”（一种简便的交通系统）中使用的底层电源结构。HUB本质上是安装在MAP的上部结构的组合单元。</p>
<p>MAP的主要目标是使静态结构可移动，从而扩展其功能。它的设计与标准集装箱兼容。MAP集成集装箱可实现无限定制功能的自动驾驶空间。</p>
<p>MAP驱动货箱内部可用于包括但不限于商店、咖啡馆、餐厅、酒店和办公室。外部也可以根据需要进行改造。有一个例子是，在其屋顶可升级为带有太阳能电池板的屋顶，以提供多功能和充电的车辆电池。</p>
<p>通过MAP集成，停车场可以很容易地转换成移动高层。这实现了牛仁电动汽车利用车辆技术提供便利、节约资源、将社区转变为高效基础设施的使命。
</p>
<p>城市基础设施需要一个能够有效连接人与服务的系统，而MAP则是整体解决方案的开始。</p>
<p>它可以把一个简单的集装箱转变成一个自动化移动住所。</p>
<p>它可以使停车场现代化，覆盖MAP单元，并将其改造为一个移动高层。</p>
<p>它可以把一个购物中心和那座移动高楼结合起来。</p>
<p>有了MAP，一个不受欢迎的停车场可以奇迹般地变成一个蓬勃发展的社区。</p>
<p>这项技术是构建下一代生态系统，实现智慧城市愿景不可或缺的一部分。这是一种新的交通模式，旨在开启智能旅行的未来。
</p>
<p>“多功能自动化平台是交通基础设施领域的一项颠覆性突破。我们公司的宗旨是通过交通运输对社会产生积极的影响。牛仁电动汽车首席执行官爱德华.李（Edward Lee）如是说。</p>
<p>牛仁电动汽车目前正在开发新能源汽车，计划于2019年第四季度推出。</p>
<p>获取更多详情，请联系  <a href="mailto:pr@neuronev.co">pr@neuronev.co </a>  </p>
<h5>公关新闻专线来源 </h5>
<a href="https://www.prnewswire.com/news-releases/neuron-ev-unveils-the-future-of-smart-travel-and-residence-300880495.html">https://www.prnewswire.com/news-releases/neuron-ev-unveils-the-future-of-smart-travel-and-residence-300880495.html</a>
<h5>相关链接 </h5>
          <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
         
        </div>
        <Footer/>
      </>
    )
  }
}

export default MapPress