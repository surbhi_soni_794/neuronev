import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class HubPress extends React.Component {
  componentDidMount() {
    document.title = "Hub Press - Neuron EV";
  }

  render() {
    return (
      <>
      <div>
        <RespHeader />
        </div>
      <Header />
        <PressMain />
        <div className="press-one-container press-page">
          <p>城市基础设施需要智能交通系统。HUB是整体解决方案的开始。HUB集成了可再生能源、电信和云计算，易于出行。HUB可变为商业、公共和私家车等多功能于一体的车型。HUB通过三种方式激活服务：按需、预订和流量检测。用户还可以通过智能设备或访问附近HUB站用GPS来定位HUB。在社区半径5英里内，低速自动驾驶共享HUB。HUB通过区块链技术集成来推动简便的共享驾驶。输入或扫描区块链ID便可享受HUB服务，无需预约，并对经常使用者提供奖励。流量检测软件将引导HUB到需求量大的区域，并预留时间来进行有效维护。HUB是多功能交通运输工具，具有多种自动化应用：</p>
          <ul className="hubPress-lists">
            <li>货物运输</li>
            <li> 储物柜</li>
            <li>急救车</li>
            <li>商业空间</li>
            <li>共享驾驶</li>
            <li>住宿</li>
            <li> 穿梭服务</li>
            <li>车队租凭</li>
            <li>私家车</li>
          </ul>
          <p>“HUB”可作为私家车接受预定。用户可以选择基本、舒适和高级等不同的居住设施。“HUB”基本款可承载6名乘客，空间舒适，且货物空间充足。</p>
          <p>牛仁电动汽车的“统一”哲学理念强调通过多样性来丰富人类生活。HUB的标志性设计融合了多种功能。电机融合车轮动力形成宽敞的八角形结构。HUB的平台可以在较低的位置实现稳定的重心，并实现高效的感应充电。镀锌外壳通过太阳能提供二次充电。标志性的电路头灯突出了HUB的标志性呈现，使其在白天和夜晚穿梭自如。</p>
          <p>HUB是交通创新的愿景，是为了推动人类进步和节约资源。牛仁电动汽车的目标是通过不断发展的智能交通在社会上产生积极的影响。牛仁电动汽车目前正致力于为不久的将来开发清洁能源汽车。</p>
          <p>获取更多详情，请联系  <a href="mailto:pr@neuronev.co">pr@neuronev.co </a>  </p>
          <h5>公关新闻专线来源 </h5>
          <a href="https://www.prnewswire.com/news-releases/neuron-ev-unveils-hub-a-new-paradigm-of-transportation-300847903.html?tc=eml_cleartime">https://www.prnewswire.com/news-releases/neuron-ev-unveils-hub-a-new-paradigm-of-transportation-300847903.html?tc=eml_cleartime</a>
          <h5>相关链接 </h5>
          <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
         </div>
         <Footer/>
      </>
    )
  }
}

export default HubPress

