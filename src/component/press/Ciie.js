import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
class Ciie extends React.Component {
  componentDidMount() {
    document.title = "CIIE 2019 - Neuron EV";
  }

  render() {
    return (
      <>
      <div><RespHeader /></div>
      <Header />
        <PressMain />
        <div className="press-one-container press-page">
          <p>牛仁在全球发布了两款令人震撼的新型电动汽车，这也引起了包括新华社在内的各大媒体的关注，新华社是中国最大、最具影响力的媒体机构，也是全球最大的通讯社。</p>
          <p>
            <p>来自加州的电动汽车公司表示，他们感到非常荣幸并且受到热烈的欢迎。虽然他们最初担心会有语言上的障碍，但他们的产品代表了他们，观众自然而然的理解了他们的使命，营造出浓浓的参与氛围。</p>
            <p>一项早期的调查显示，大多数的参与者都愿意支付定金来预定牛仁T-ONE。受访者大多被外部/内部设计、内部空间感及其作为商务用车的潜在多功能用途所吸引。 </p>
<p>牛仁电动汽车品牌专家Casey Hyun说：“能收到这么积极的相应真的是件令人荣幸的事情。我们了解到客户对产品非常感兴趣，有些顾客甚至想当场对两款车下单。许多汽车展商屡次来到展台，表示祝贺。“
</p>
            <p>牛仁电动汽车在视觉、品牌和产品方面创造了一种耳目一新的独特性，汽车公司推出的产品已被大众所熟知，人们在看到牛仁的时候却发现了另一片新天地。</p>
            <p>“牛仁和观众之间有一种真正的联系，”牛仁电动汽车空间设计师莉迪亚•李（Lydia Li）说，“由于我们对实现可持续发展未来保持着脚踏实地的态度，所以牛仁能成为聚光灯下的焦点也是可预见的，令人成就感十足。我们仅做我们自己。能成为我们公司这一历史性时刻的一员，真的令人感到激动。” </p>
<p>牛仁电动汽车将于中国国际进口博览会后迁至虹桥进口商品展示交易中心展示车辆，展览将于12月2日开始对外开放。</p>


            获取更多详情，请联系 <a href="mailto:pr@neuronev.co">pr@neuronev.co </a> </p>
<h5>公关新闻专线 </h5>
        <p>
          <a href="https://www.prnewswire.com/news-releases/neuron-ev-drives-into-the-spotlight-at-ciie-300961923.html">https://www.prnewswire.com/news-releases/neuron-ev-drives-into-the-spotlight-at-ciie</a>
        </p>

        <h5>相关链接 </h5>
        <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
      </div>
      <Footer />
      </>
    )
  }
}

export default Ciie