import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class PressTwo extends React.Component {
  componentDidMount() {
    document.title = "12-06-19 - Neuron EV";
  }
  render() {
    return (
      <>
      <div><RespHeader /></div>
       <Header />
        <PressMain />
        <div className="press-one-container press-page">
      
          <p>
          牛仁电动汽车TORQ是半挂车向电动汽车时代过渡过程中的一次重大飞跃。得益于牛仁的电池电力推进系统，它既满足了传统半挂车的需求和期望，又不会产生化石燃料的有害影响。
          TORQ的创立是为了消除温室气体排放，并为商业交通行业开创一个更清洁的未来。</p>
          <h2>功能</h2>
          <p> TORQ有一个可伸缩的底盘，带有可互换的车身部件，易于安装和拆卸，使其能够进行功能调整和增加载货能力。
          半挂车还具有空气动力轮廓的功能性结构，包括集成的卧铺车厢，带有一个光滑和坚固的外壳，以实现静音操作。
                  </p>  <h2>安全</h2>

          <p> TORQ驾驶座具有全景360度视野，这应归功于向驾驶员屏幕提供可视数据的无线摄像头。这一颠覆性功能可确保驾驶员可以随时从各个角度了解车辆的位置和周围环境。
          驾驶员中心座椅可提供可视性平衡、优化操控性和给人一种超棒驾驶体验。
          半挂车直观的车辆接口应用通过任意个人设备提供全面操作控制和运行，插入内部结构时，可无缝取代所有物理旋钮和开关。简洁的开放式系统与最新小工具和互联网解决方案兼容。
                        </p>    <h2>便捷</h2>

          <p> 牛仁电动汽车还解决了长途司机的困境。在长途驾驶时，当他们中途休息时会需要一个舒适的休息区。TORQ配备了一个现代化的头等舱，既适合驾驶，也有利于休息和睡觉。
          后部睡觉区域带有环绕音响系统、丰富的存储空间、互联网连接和豪华床，可供电影娱乐。
          舱室与未来的自动化操作兼容。
                              </p>     <h2>性能</h2>

          <p>TORQ拥有重型电力传动系统性能。更重要的是，电动汽车长途行驶技术现已基本成熟，能够在一次充电的情况下满足长距离行驶。它们也更节能，每英里消耗更少的能源。
                                </p>        <h2>低维护</h2>

          <p> 电动汽车需要较少的维护，跟拥有数百个工作部件的内燃机相比，电动机仅包含大约六个运动部件。随着时间推移，可以了解到电动机部件是耐用且易于更换的。
          牛仁设想其可再生能源广场（Renewable Energy Plaza）或REP是一个综合性的服务中心，用户可以在这里可获得维护和电池更换服务方面的支持。
          TORQ是牛仁电动汽车倡导的简约和功能性理念的最好呈现。易驾驶，具有普遍适用性 ，可升级到最新技术，高效减少排放物，并优化多功能性。
              </p><p>垂询详情，请联系<a href="mailto:pr@neuronev.co">pr@neuronev.co </a>
              </p><h5>美通社链接 </h5> 
          <a href="https://www.prnewswire.com/news-releases/neuron-ev-introduces-pure-electric-torq-300970545.html">https://www.prnewswire.com/news-releases/neuron-ev-introduces-pure-electric-torq</a>
          
          <h5> 相关链接 </h5>
          <a href="https://www.youtube.com/watch?v=RdqOaqPx7T4">https://www.youtube.com/watch?v=RdqOaqPx7T4</a>
          <br /> <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
          
        </div>
        <Footer/>
      </>
    )
  }
}

export default PressTwo