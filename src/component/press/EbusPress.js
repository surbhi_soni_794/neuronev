
import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class EbusPress extends React.Component {
  componentDidMount() {
    document.title = "Ebus Press - Neuron EV";
  }

  render() {
    return (
      <>
      <div><RespHeader /></div>
      <Header />
        <PressMain />
        <div className="press-one-container press-page">
    
          <p>Now, the Neuron Electric Bus builds a bridge that will ultimately take us to a clean, green, and smart public transportation.</p>
          <p>The electric bus is completely modular, which means the length of the vehicle can be modified to serve in a variety of ways, taking on different forms like a mini bus, transit bus, or an articulated bus that can be used for diverse purposes for communities and infrastructures.</p>
          <p>The Neuron Electric Bus is made to be compatible with high-speed railways—it's essentially a bus that can convert into a fast metro train. This technology is truly a monumental innovation in modular versatility for automobiles.</p>
          <p>Neuron's clean energy bus can be operated by either a driver or as an autonomous vehicle, and its front portion is interchangeable to meet aerodynamic requirements.</p>
          <p>While the idea of using electricity to power buses is nothing new and has been explored for more than a hundred years, it was only in recent years that the technology became mature enough to allow for practical implementation and mass production.</p>
          <p>Though about 17 percent of the world's buses are now electric, 99 percent of these green transports are in China. The Asian powerhouse nation accounts for more than 425,000 of these buses, while the U.S. only has a little more than 300 units.</p>
          <p>This is all set to change after Neuron EV's entry into the market. This development couldn't have come at a better time, as more and more states and cities are beginning to adopt the new technology. California leads the way, even mandating that all the state's buses purchased by its mass transit agencies should be zero-emission by the year 2029.</p>
          <p>Neuron EV clearly recognizes that electric commercial vehicles are the future, and with its talented personnel and relentless research, they are set to be at the forefront of this new transport industry.</p>
          <p>Please contact <a href="mailto:pr@neuronev.co">pr@neuronev.co </a> for more information.
          </p><h5>SOURCE PR Newswire </h5>
          <a href="https://www.prnewswire.com/news-releases/neuron-ev-steers-into-the-future-with-new-electric-bus-300898554.html">https://www.prnewswire.com/news-releases/neuron-ev-steers-into-the-future-with-new-electric-bus-300898554.html</a>
          <h5>Related Links </h5>
          <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
          
        </div>
        <Footer/>
      </>
    )
  }
}

export default EbusPress