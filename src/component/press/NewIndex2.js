import React from 'react';
import PressMain from './PressMain';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class NewIndex2 extends React.Component {
    componentDidMount() {
        document.title = "Rep Press - Neuron EV";
    }

    render() {
        return (
            <>
            <div><RespHeader /></div>
             <Header />
                <PressMain />
                <div className="press-one-container press-page">
                    <p>牛仁电动公共汽车是商用车行业的一种新解决方案，它提供了一种令人愉悦的乘坐体验，使您能够工作、放松并享受到达目的地的途中的乐趣。公共汽车提供多种宽敞的设计，可配有豪华住宿和私人卧室，可通过互联网进行预订。</p>
                    <p>豪华公共汽车配有一个社交休息室、一个电影院综合娱乐空间、睡眠区、商务工作站和两个浴室，均采用优质环保材料制成。这是因为牛仁想要创造一种新的移动体验，一种通常在现代家庭、豪华酒店或机动化平台上的休息室俱乐部中享有的体验。</p>
                    <p>这似乎还不够具有颠覆性，牛仁还将彻底改变充电站的体验，推出可再生能源广场（Renewable Energy Plaza）或REP。</p>
                    <p>这是一个引人注目集技术与休闲于一体的服务中心，人们在这里一边享受时光，一边为他们的电动汽车充电。这是一个自身可持续的能源中心，可连接其他“REP”，建立模块化易于扩展。“牛仁可再生能源广场是关于整合，这是智能城市发展的预览。”牛仁电动汽车首席执行官爱德华.李（Edward Lee）如是说。</p>
                    <p>REP通过多种方式提供能源：太阳能电池板发电，燃料电池存储的电能和发电机的无线快速充电。 它还可以提供维护服务和电池更换。</p>
                    <p>该中心的特点是将食品、饮料和娱乐融合在一个有趣的虚拟购物中心内，通过带有直播购物的大型数字化屏幕进行浏览和交易。它有一个无人送货系统，人们可以在那里购物和接收货物，享受快捷服务。</p>
                    <p>REP同时也是一个体验中心，用于试驾和了解更多关于牛仁电动汽车的信息，目的是帮助人们更多地了解环保技术。</p>
                    <p>获取更多详情，请联系  <a href="mailto:pr@neuronev.co">pr@neuronev.co </a>  </p>
                    <h5>公关新闻专线来源 </h5>
                    <a href="https://www.prnewswire.com/news-releases/neuron-ev-expands-the-renewable-energy-transportation-landscape-300907430.html">https://www.prnewswire.com/news-releases/neuron-ev-expands-the-renewable-energy-transportation-landscape-300907430.html</a>
                    <h5>相关链接 </h5>
                    <a href="https://www.instagram.com/neuron_ev/">https://www.instagram.com/neuron_ev/</a><p></p>
                   
                </div>
                <Footer/>
            </>
        )
    }
}

export default NewIndex2