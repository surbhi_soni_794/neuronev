import React from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import {slidingImages} from '../../store/actions/index';
import $ from 'jquery';


class MapSection extends React.Component {
    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 800;
            slidingImages(duration, fadeAmount, durationTime);
            
        })
    }



    componentDidMount(){
            document.title= "MAP - Neuron EV";
            if(document.body.querySelector('.star-main-wrapper'))
            document.body.querySelector('.star-main-wrapper').addEventListener('scroll', this.aosInit);
    }
    aosInit(){
        window.AOS.init({
          duration: 2000,
        });
      }

          goToSection =() => {
            var elmnt = document.getElementById("map-video");
            elmnt.scrollIntoView();
          }

          goToBottom =() => {
            var elmnt = document.getElementById("map-video");
            elmnt.scrollIntoView();
          }
          goToTop =() => {

            var elmnt = document.getElementById("top-id");
            elmnt.scrollIntoView();
          }

          enterFirst = () => {

            if(document.getElementsByClassName('first-val').length > 0)
             document.getElementsByClassName('first-val')[0].style.visibility = 'initial';
          }
          enterSecond = () => {

            if(document.getElementsByClassName('second-val').length > 0)
             document.getElementsByClassName('second-val')[0].style.visibility  = 'initial';
              
        }
        leaveFirst = () => {
            if(document.getElementsByClassName('first-val').length > 0 )
             document.getElementsByClassName('first-val')[0].style.visibility = 'hidden';
        }

        leaveSecond = () => {
            if(document.getElementsByClassName('second-val').length > 0 )
              document.getElementsByClassName('second-val')[0].style.visibility = 'hidden';
        }


    render() {
        return (
            <div>
                <div className="cursor-pointing">
                <div className="dash-container"   onClick={this.goToTop}><span  className="dash-value first-val">1</span><span onMouseEnter={this.enterFirst}   onMouseLeave={this.leaveFirst}  className="map-dash"></span></div>
                <div className="dash-container"  onClick={this.goToBottom}><span className="dash-value second-val">2</span> <span onMouseEnter={this.enterSecond}  onMouseLeave={this.leaveSecond} className="map-dash"></span></div>                
                </div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <div id="top-id" className="parallax-image">
                    <div className="parallax-image-content">
                        <div>自动机动化平台</div>
                        <div className='maps-arrow '>
                            <img onClick={this.goToSection} src="http://cdn.cn.neuronev.co/assets/images/arrow1.png" alt="arrow" />
                        </div>
                    </div>
                </div>
                <section className="map-parallax-img-wrapper parallax bg1 new-height-map">  </section>
                <div className="mapping-section map-head">
                    <span id="map-video" className="map-text-wrapper">MAP</span>
                </div>
                <div  className="map-video container-fluid">
                    <div className="row vision-wrapper mt-60">
                    <iframe title="vision-video"
                            src="https://player.vimeo.com/video/354113787?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                            frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                    </iframe>
                    </div>
                </div>
                <div className="blank-section"></div>

                <div className="container-fluid map-culture-section">
                    <div className="row">
                    <div className="col-sm-1"></div>
                        <div className="col-sm-7 map-culture-img">
                            <img src="http://cdn.cn.neuronev.co/assets/images/map/5 (3).jpg" data-aos="fade-right" id="image-map-c1" className="image-map" alt="map_culture" />
                        </div>
                        <div className="col-sm-3 map-culture-content">
                            <div className="map-culture-content-heading">
                            促进人类进步的智能解决方案
                        </div>
                            <div className="map-culture-content-text">
                            多功能型自动平台或 “MAP”，一种安装各种尺寸结构的自动机动化平台，以便将它们从一个位置运输到另一个位置。它的大小可以根据具体的工作进行相应调整，例如可作为乘客的交通工具、货物的运输工具或旅行者的临时住所等等。
                        </div>
                        </div>
                    </div>
                </div>


                <div className="border-line-map border-map-sec"></div>
                <div className="map-smart-purpose container">
                    <div className="row">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-6 smart-purpose-container map-top-content">
                            <div className="map-smart-heading">智能目的</div>
                            <div className="map-smart-content">
                            MAP™（模块化自动平台）是牛仁“HUB”（一种简便的交通系统）中使用的底层电源结构。HUB本质上是安装在MAP的上部结构的组合单元。MAP的主要目标是使静态结构可移动，从而扩展其功能。它的设计与标准集装箱兼容。MAP集成集装箱可实现无限定制功能的自动驾驶空间。
                        </div>
                        </div>
                        <div className="col-sm-3"></div>
                    </div>
                </div>
                <div className="map-image-one container">
                    <img id="map-image-1" src="http://cdn.cn.neuronev.co/assets/images/map/map1.png" data-aos="fade-up" data-aos-anchor-placement="top-bottom"  alt="img-one" />
                </div>
                <div className="energy-creation-map container">
                    <div className="row">
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8">
                            <div className="zero-map-head">零排放，从能源创造到消费</div>
                            <div className="list-energy container">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <div className="map-energy-sub">清洁环境</div>
                                        <ul className="listing-energy">
                                            <li><span>•</span> 无废气</li>
                                            <li>• 5倍效率<span className="hub-font-wrapper">vs ICE</span></li>
                                            <li>• 生活能源</li>
                                            <li>• 静音操作</li>
                                        </ul>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="map-energy-sub">节约资源</div>
                                        <ul className="listing-energy">
                                            <li>• 充电灵活</li>
                                            <li>• 太阳能可再生能源</li>
                                            <li>• 可持续材料</li>
                                            <li>• 连接驱动</li>
                                        </ul>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="map-energy-sub">多功能电源</div>
                                        <ul className="listing-energy">
                                            <li>• 模块化效率</li>
                                            <li>• 开放式动力系统</li>
                                            <li>• 强大的扭矩</li>
                                            <li>• 范围广泛</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>
                </div>

                <div className="map-image-two">
                    <img id="map-image-2" src="http://cdn.cn.neuronev.co/assets/images/map/map2.png" data-aos="fade-left" alt="img-one" />
                </div>
                <div className="map-image-three map-sec-images">
                    <img id="map-image-3" src="http://cdn.cn.neuronev.co/assets/images/map/map3.png" alt="img-one" />
                </div>
                <div className="container-fluid section-divider-map">
                    <div className="row">
                        <div className="col-sm-6">
                            <img id="map-image-4" src="http://cdn.cn.neuronev.co/assets/images/map/2 (3).jpg" data-aos="fade-right" style={{ height: '100%'}} alt="img-one" />

                        </div>
                        <div className="col-sm-6">
                            <img id="map-image-5" src="http://cdn.cn.neuronev.co/assets/images/map/6 (4).jpg" data-aos="fade-left" alt="img-one" />
                        </div>
                    </div>
                </div>

                <div className="border-line-map-2 border-map-sec"></div>

                <div className="map-smart-purpose container" style={{ marginBottom: '3%', zIndex: 0}}>
                    <div className="row">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-6 smart-purpose-container" style={{ color: '#ffffffcf'}}>
                            <div className="map-smart-heading">MAP的多功能用途</div>
                            <div className="map-smart-content" style={{ padding: '0 20px'}}>
                            MAP驱动货箱内部可用于包括但不限于商店、咖啡馆、餐厅、酒店和办公室。外部也可以根据需要进行改造。有一个例子是，在其屋顶可升级为带有太阳能电池板的屋顶，以提供多功能和充电的车辆电池。
                        </div>
                        </div>
                        <div className="col-sm-3"></div>
                    </div>
                </div>
                <div className="map-image-four">
                <img id="map-image-6" src="http://cdn.cn.neuronev.co/assets/images/map/pod1_9574.png" data-aos="fade-up" data-aos-anchor-placement="top-bottom" alt="img-one" />
                </div>
                    
                <div className="smart-mobility-text">
                智能移动
                </div>
                <div className="map-mobility-images container-fluid map-bottom-sec">
                    <div id="slideshow" className="w3-content w3-section map-sliding-images map-height">
                        <img className="map-slides-image" alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/map/7 (4).jpg" />
                        <img className="map-slides-image" alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/map/8 (4).jpg" />
                        <img className="map-slides-image" alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/map/resized.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/map/5 (3).jpg" />
                    </div>
                </div>
                <Footer/>
             
             </main>
             </div>

        )
    }
}

export default MapSection