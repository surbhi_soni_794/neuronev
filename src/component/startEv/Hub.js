import React from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import {slidingImages} from '../../store/actions/index';
import $ from 'jquery';

class HubSection extends React.Component { 
    constructor(props) {
        super(props);
        $(document).ready(() => {
            let duration = 18;
            let fadeAmount = 0.3;
            let durationTime = 1000;
            slidingImages(duration, fadeAmount, durationTime);
            
        })
    }
    
    componentDidMount(){
        document.title= "HUB - Neuron EV";
        if(document.body.querySelector('.star-main-wrapper'))
        document.body.querySelector('.star-main-wrapper').addEventListener('scroll', this.aosInit);
          }
          aosInit(){
            window.AOS.init({
              duration: 2000,
            });
          }

          goToSection =() => {
            var elmnt = document.getElementById("map-video");
            elmnt.scrollIntoView();
          }

          goToBottom =() => {
            var elmnt = document.getElementById("map-video");
            elmnt.scrollIntoView();
          }
          goToTop =() => {

            var elmnt = document.getElementById("top-id");
            elmnt.scrollIntoView();
          }

          enterFirst = () => {
            if(document.getElementsByClassName('first-val').length > 0)
             document.getElementsByClassName('first-val')[0].style.visibility = 'initial';
          }
          enterSecond = () => {
            if(document.getElementsByClassName('second-val').length > 0)
             document.getElementsByClassName('second-val')[0].style.visibility  = 'initial';
              
        }
        leaveFirst = () => {
            if(document.getElementsByClassName('first-val').length > 0 )
             document.getElementsByClassName('first-val')[0].style.visibility = 'hidden';
        }

        leaveSecond = () => {
            if(document.getElementsByClassName('second-val').length > 0 )
              document.getElementsByClassName('second-val')[0].style.visibility = 'hidden';
        }


    render() {
        return (
            <div>
                <div className="cursor-pointing">
                <div className="dash-container"   onClick={this.goToTop}><span  className="dash-value first-val">1</span><span onMouseEnter={this.enterFirst}   onMouseLeave={this.leaveFirst}  className="map-dash"></span></div>
                <div className="dash-container"  onClick={this.goToBottom}><span className="dash-value second-val">2</span> <span onMouseEnter={this.enterSecond}  onMouseLeave={this.leaveSecond} className="map-dash"></span></div>                
                </div>
                <RespHeader />
                <main className="star-main-wrapper">
                <Header /> 
                <div id="top-id" className="parallax-image-hub">
                    <div className="parallax-image-content-hub">
                        <div className="hub-heading-content">新型交通运输典范
                        </div>
                        <div className='hub-arrow'>
                            <img id="drop-down-image" onClick={this.goToSection} src="http://cdn.cn.neuronev.co/assets/images/arrow1.png"  alt="arrow" />
                        </div>
                    </div>
                </div>
                <section className="map-parallax-img-wrapper parallax bg1 new-hub-height">  </section>
                <div className="mapping-section-hub">
                    <span id="map-video" className="map-text-wrapper">HUB</span>
                </div>
                <div  className="map-video container-fluid">
                    <div className="row hub-video-wrapper">
                        <iframe src="https://player.vimeo.com/video/354291998?background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
                        frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow:'hidden', height:'100%',width:'100%'}} height="100%" width="100%">
                        </iframe>
                    </div>
                </div>
                 <div className="blank-section"></div>

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8 ">
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm-8 hub-image-container-one ">
                                        <img src="http://cdn.cn.neuronev.co/assets/images/hub/NeuronEV_2.jpg" data-aos="fade-right" id="image-map-c1" alt="hub_culture" />
                                    </div>
                                    <div className="col-sm-4 ">
                                        <div className="hub-culture-content">
                                            <div className="hub-culture-content-heading">
                                            和谐、
                        </div>
                        <div className="hub-culture-content-heading"> 多功能&平衡</div>
                                            <div className="hub-culture-content-text">
                                            牛仁 “HUB” 代表和谐、多功能、平衡。 “HUB”是一个简便的交通系统，以促进人类发展和保护我们的环境为宗旨。
                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="col-sm-2"></div>

                    </div>
                </div>


              <div className="border-line-hub"></div>
                 <div className="map-smart-purpose container">
                    <div className="row">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-6 smart-purpose-container hub-image-content">
                            <div className="map-smart-heading">可持续环境</div>
                            <div className="map-smart-content">
                            推进人类发展的蓝图始于废物最小化。牛仁致力于推动技术创新，为我们的环境提供可持续的解决方案。
                        </div>
                        </div>
                        <div className="col-sm-3"></div>
                    </div>
                </div>
                <div className="hub-image-one container-fluid">
                
                    <img src="http://cdn.cn.neuronev.co/assets/images/hub/hub.png" data-aos="fade-up" data-aos-duration="3000" id="image-hub-h1" alt="img-one" />
                </div>
                <div className="energy-creation-map container energy-creation-hub">
                    <div className="row">
                        <div className="col-sm-2"></div>
                        <div className="col-sm-8">
                            <div className="zero-map-head">零排放，从能源创造到消费</div>
                            <div className="list-energy container">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <div className="map-energy-sub">清洁环境</div>
                                        <ul className="listing-energy">
                                            <li>• 无废气</li>
                                            <li>• 5倍效率<span className="hub-font-wrapper">vs ICE</span></li>
                                            <li>• 生活能源</li>
                                            <li>• 静音操作</li>
                                        </ul>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="map-energy-sub">节约资源</div>
                                        <ul className="listing-energy">
                                            <li>• 充电灵活</li>
                                            <li>• 太阳能可再生能源</li>
                                            <li>• 可持续材料</li>
                                            <li>• 连接驱动</li>
                                        </ul>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="map-energy-sub">多功能电源</div>
                                        <ul className="listing-energy">
                                            <li>• 模块化效率</li>
                                            <li>• 开放式动力系统</li>
                                            <li>• 强大的扭矩</li>
                                            <li>• 范围广泛</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-2"></div>
                    </div>
                </div>

               <div className="map-image-two">
                    <img src="http://cdn.cn.neuronev.co/assets/images/hub/pod1_9573_copy.png" data-aos="fade-left" alt="img-one" id="image-hub-h2"/>
                </div>
                <div className="map-image-three hub-sec-images">
                    <img src="http://cdn.cn.neuronev.co/assets/images/hub/hub3.png" data-aos="fade-up" alt="img-one" />

                </div>

                <div className="container-fluid section-divider-map">
                    <div className="row">
                        <div className="col-sm-6">
                            <img id="hub-section-one" src="http://cdn.cn.neuronev.co/assets/images/hub/frontN.jpg" data-aos="fade-right" style={{ height: '100%'}} alt="img-one" />

                        </div>
                        <div  className="col-sm-6">
                            <img id="hub-section-two" src="http://cdn.cn.neuronev.co/assets/images/hub/n_camp.jpg" data-aos="fade-left" alt="img-one" />
                        </div>
                    </div>
                </div>

                <div className="border-line-map-2 hub-sec"></div>

             <div className="map-smart-purpose container" style={{ marginBottom: '3%', zIndex: 0}}>
                    <div className="row">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-6 smart-purpose-container" style={{ color: '#ffffffcf'}}>
                            <div className="map-smart-heading">智能目的</div>
                            <div className="map-smart-content" style={{ padding: '0 20px'}}>
                            “HUB”是一款多功能交通工具，具有多种自动化应用。“HUB”可作为私家车接受预定。用户可以选择基本、舒适和高级等不同的居住设施。“HUB”基本款可承载6名乘客，空间舒适，且货物空间充足。
                        </div>
                        </div>
                        <div className="col-sm-3"></div>
                    </div>
                </div>
                <div className="map-image-four">
                <img id="map-image-6" src="http://cdn.cn.neuronev.co/assets/images/hub/hub4.png"  data-aos="fade-up" data-aos-duration="3000" alt="img-one" />
                </div>
                <div className="smart-mobility-text">
                智能移动
                </div>
                <div className="map-mobility-images container-fluid hub-mobility-images">
                    <div  id="slideshow" className="w3-content w3-section map-sliding-images hub-slider-wrapper">
                        <img className="map-slides-image" alt="img-1" src="http://cdn.cn.neuronev.co/assets/images/hub/N_Rear.jpg" />
                        <img className="map-slides-image" alt="img-2" src="http://cdn.cn.neuronev.co/assets/images/hub/delivery.jpg" />
                        <img className="map-slides-image" alt="img-3" src="http://cdn.cn.neuronev.co/assets/images/hub/enter.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/hub/N_factory2.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/hub/n_park2.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/hub/NeuronEV_6.jpg" />
                        <img className="map-slides-image" alt="img-4" src="http://cdn.cn.neuronev.co/assets/images/hub/n12.jpg" />
                    </div>
                </div>
                <Footer/>
             
             </main>
            </div>

        )
    }
}

export default HubSection