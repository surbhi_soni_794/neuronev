import React, { Component } from 'react';

class SocialIcon extends Component {

  openSocialPage = (val) => {
    switch (val) {
      case 'instagram': window.open('https://www.instagram.com/neuron_ev/'); break
      case 'facebook': window.open('https://www.facebook.com/neuronev/'); break
      case 'linkdein': window.open('https://www.linkedin.com/company/neuron-ev'); break
      case 'twitter': window.open('https://twitter.com/neuron_ev/'); break
      case 'youtube': window.open('https://www.youtube.com/channel/UC69MUFfq-4W8qxUWcGltBUg/'); break
    default: window.open('https://www.instagram.com/neuron_ev/');
    }

  }
  render() {
    return (
      <ul className="list-unstyled social-icon my-own-socialism">
        <li><i onClick={() => this.openSocialPage('instagram')} className="fa fa-instagram"></i></li>
        <li><i onClick={() => this.openSocialPage('facebook')} className="fa fa-facebook-f"></i></li>
        <li><i onClick={() => this.openSocialPage('linkdein')} className="fa fa-linkedin"></i></li>
        <li><i onClick={() => this.openSocialPage('twitter')} className="fa fa-twitter"></i></li>
        <li><i onClick={() => this.openSocialPage('youtube')} className="fa fa-youtube"></i></li>
      </ul>
    )
  }
}

export default SocialIcon;