import React from 'react'

const Buttons = ({title = '', clsName, onFormSubmit, onEnterSubmit }) => (
    <div className={clsName}>
        <button onClick={onFormSubmit} onKeyDown={onEnterSubmit} name="button" type="submit">{title}</button>
    </div>
)
export default Buttons