import React, { Component } from 'react';

class FormError extends Component {
    render() {
        return (
            <div className="error-conatiner">
                <i className="fa fa-times cross-icon" aria-hidden="true"></i>
                {this.props.error}
            </div>
        )
    }
}

export default FormError