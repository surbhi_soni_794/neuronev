import React, { Component } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Tech extends Component {
    componentDidMount(){
        document.title= "TECH — Neuron EV";
    }
    render() {
        return (
            <div className="vision-container">
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <div className="vision-video-section">
                    <div className="row" className="vision-wrapper mt-60">
                        <iframe title="vision-video"
                            src="https://player.vimeo.com/video/355179915?background=1&autoplay=1&loop=1&color=ffffff&title=0&byline=0&portrait=0"
                            frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                        </iframe>
                    </div>
                </div>
                <div className="vision-content tech-content">
                <h1>牛仁电动汽车卓越的质量标准源自先进的技术和精密的工艺。 </h1></div>
                    <Footer/>
                </main>
            </div>
        )
    }
}

export default Tech

