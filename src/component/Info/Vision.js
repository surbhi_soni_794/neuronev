import React, { Component } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Vision extends Component {
    componentDidMount(){
        document.title= "ABOUT — Neuron EV";
    }
    render() {
        return (
            
            <div className="vision-container">
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <div className="vision-video-section">
                    <div className="row" className="vision-wrapper mt-60">
                        <iframe title="vision-video"
                            src="https://player.vimeo.com/video/355200297?background=1&autoplay=1&loop=1&color=ffffff&title=0&byline=0&portrait=0"
                            frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                        </iframe>
                    </div>
                </div>
                <div className="vision-content"> <h1>欢迎来到牛仁电动汽车，通过电动汽车实现人、技术与环境的协同。</h1></div>
            <Footer/>
                </main>
            </div>
        )
    }
}

export default Vision