import React, { Component } from 'react';
import FormError from './FormError';
import Buttons from './Buttons';
import sgMail from '@sendgrid/mail';

// const sgMail = require('@sendgrid/mail');


var Api_Key = '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';
var Template_ID = 'd-bb33d528e5324030b6f632f20e126456';

class ContactForm extends Component {
    state = {
        firstName: '',
        lastName: '',
        email: '',
        subject: '',
        message: '',
        errorField: [],
        isFromSubmit: false,
        topBottomError: false,
        isFirstNameError: false,
        isEmailError: false,
        isSubjectError: false,
        isMessageError: false,
        isEmailValidateError: false
    }

    sendMail = () => {
        const { email, message, subject } = this.state;

        sgMail.setApiKey(Api_Key);

        const msg = {
            to: 'abraham@tepia.co',
            from: email,
            subject: subject,
            text: message,
            // templateId: 'd-bb33d528e5324030b6f632f20e126456',
        };
        sgMail.send(msg);
    }

    onEnterSubmit = (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            event.stopPropagation();
            this.onFormSubmit();
        }
    }

    onFormSubmit = (e) => {
        const { firstName, lastName, email, subject, message } = this.state
        e.preventDefault();

        if (!this.props.section && firstName == '' && lastName == '' && email == '' && subject == '' && message == '') {
            this.setState({ topBottomError: true, isFirstNameError: true, isEmailError: true, isSubjectError: true, isMessageError: true })
        }
        if (this.props.section && firstName == '' && lastName == '' && email == '' && subject == '' && message == '') {
            this.setState({ topBottomError: true, isFirstNameError: true, isEmailError: true, isSubjectError: false, isMessageError: true })
        }

        if (this.props.section && (firstName == '' || lastName == '' || email == '' || subject == '' || message == '')) {
            this.setState({ topBottomError: true, isFirstNameError: true, isEmailError: true, isSubjectError: false, isMessageError: true })
        }

        if (!this.props.section && (firstName == '' || lastName == '' || email == '' || subject == '' || message == '')) {
            this.setState({ topBottomError: true, isFirstNameError: true, isEmailError: true, isSubjectError: true, isMessageError: true })
        }
        if (firstName !== '' && lastName !== '') {
            this.setState({ isFirstNameError: false, })
        }
        if (email !== '') {
            this.setState({ isEmailError: false })
        }
        if (subject !== '') {
            this.setState({ isSubjectError: false })
        }
        if (message !== '') {
            this.setState({ isMessageError: false })
        }

        if (!this.props.section && firstName !== '' && lastName !== '' && email !== '' && subject !== '' && message !== '') {
            this.sendMail()
            this.setState({ isFromSubmit: true, topBottomError: false, isFirstNameError: false, isEmailError: false, isSubjectError: false, isMessageError: false })
        }
        if (this.props.section && firstName !== '' && lastName !== '' && email !== '' && message !== '') {
            this.sendMail()
            this.setState({ isFromSubmit: true, topBottomError: false, isFirstNameError: false, isEmailError: false, isSubjectError: false, isMessageError: false })
        }

    }
    onChnageHandler = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    checkEmail = (email) => {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!filter.test(email)) {
            this.setState({ isEmailValidateError: true })
            return false;
        }
        else {
            this.setState({ isEmailValidateError: false })
        }
    }

    onEmailChange = (e) => {
        this.setState({ email: e.target.value })
        this.checkEmail(this.state.email);
    }


    render() {
        const { firstName, lastName, email, subject, message, isFromSubmit, topBottomError, isFirstNameError, isEmailError, isMessageError, isSubjectError, isEmailValidateError } = this.state
        const { section } = this.props;
        if (isFromSubmit) {
            return (
                <div className="form-wrapper submit-wrapper">
                    <h1>Please complete the form below.</h1>
                    <h1>Thank you!</h1>
                </div>
            )
        }
        return (
            <div className="form-wrapper">
                {section ? null : <h1>请填写下表 </h1>}

                {topBottomError ? <FormError error="Your form has encountered a problem. Please scroll down to review." /> : null}
                {
                    section ? <div>
                        <div className="label-wrapper">
                        选择型号 *
                    </div>
                        <select className="wrapper-select">
                            <option>T.ONE</option>
                            <option>TORQ</option>
                        </select>
                    </div>
                        : null
                }
                <div className="name-section">
                    {isFirstNameError ? <FormError error="Name is required" /> : null}
                    <div className={isFirstNameError ? "label-wrapper error-color":"label-wrapper"}>
                        名字 *
                    </div>
                    <div className="input-wrapper">
                        <div className="with-label">
                            <input value={firstName} onChange={this.onChnageHandler} type="text" name="firstName" className="first-input" />
                            <label className={isFirstNameError ? "label-wrapper error-color":"label-wrapper"}>名</label>
                        </div>
                        <div className="input-fullName with-label">
                            <input value={lastName} onChange={this.onChnageHandler} type="text" name="lastName" className="first-input" />
                            <label className={isFirstNameError ? "label-wrapper error-color":"label-wrapper"}>姓 </label>
                        </div>
                    </div>
                </div>
                <div className="name-section">
                    {isEmailError ? <FormError error="Email is required" /> : null}
                    {isEmailValidateError ? <FormError error="Please Provide Valid Email conatins @ and .com" /> : null}
                    <div className={isEmailError ? "label-wrapper error-color":"label-wrapper"}>
                        邮箱 *
                    </div>
                    <div className="input-wrapper">
                        <input value={email} onChange={this.onEmailChange} type="text" name="email" className="email-input" isrequired="true" />
                    </div>
                </div>
                {section ? null : <div className="name-section">
                    {isSubjectError ? <FormError error="Subject is required" /> : null}
                    <div className={isSubjectError ? "label-wrapper error-color":"label-wrapper"}>
                        主题 *
                    </div>
                    <div className="input-wrapper">
                        <input value={subject} onChange={this.onChnageHandler} type="text" name="subject" className="full-input" />
                    </div>
                </div>}
                <div className="name-section">
                    {isMessageError ? <FormError error="Message is required" /> : null}
                    <div className={isMessageError ? "label-wrapper error-color":"label-wrapper"}>
                        留言 *
                    </div>
                    <div className="input-wrapper">
                        <textarea value={message} onChange={this.onChnageHandler} name="message" rows="4" cols="50" className="full-input">
                        </textarea>
                    </div>
                </div>
                <Buttons onEnterSubmit={this.onEnterSubmit} onFormSubmit={this.onFormSubmit} title={section ? '预定' : '提交'} clsName={section ? "button-wrapper reserve" : 'button-wrapper'} />
                {topBottomError ? <FormError error="Your form has encountered a problem. Please scroll up to review." /> : null}
            </div>
        )
    }
}

export default ContactForm