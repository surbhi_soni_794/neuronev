import React, { Component } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Social extends Component {
    componentDidMount(){
        document.title= "SOCIAL — Neuron EV";
    }
    render() {
        return (
            <div className="vision-container">
                <RespHeader />
                 <main className="star-main-wrapper">
                <Header />
                <div className="vision-video-section">
                    <div className="row" className="vision-wrapper mt-60">
                        <iframe title="vision-video"
                            src="https://player.vimeo.com/video/355152349?mw=1300&mh=732&background=1&autoplay=1&loop=1&color=ffffff&title=0&byline=0&portrait=0"
                            frameBorder="0" allow="autoplay; fullscreen" allowFullScreen={true} style={{ overflow: 'hidden', height: '100%', width: '100%' }} height="100%" width="100%">
                        </iframe>
                    </div>
                </div>
                <div className="vision-content social-content-bottom"> <h1> 改变从我做起</h1></div>
            <Footer/>
                </main>
            </div>
        )
    }
}

export default Social