import React, { Component } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';
import Calender from './Calender';

class Press extends Component {
    componentDidMount() {
        document.title = "EVENT — Neuron EV";
    }
    render() {
        return (
            <div className="press-container">
                <RespHeader />
                <main className="star-main-wrapper">
                    <Header />
                    <div className="press-wrapper">
                        <h2>牛仁电动汽车大事件</h2>
                    </div>
                    <div className="press-img">
                        <img src="http://cdn.cn.neuronev.co/assets/images/press/press.jpg" alt="press" />
                    </div>
                    <div className="press-content">
                        <p>牛仁在2019中国进口博览会展会上展示预生产车型。</p>
                        <ul data-rte-list="default">
                            <li><p><a href="https://www.prnewswire.com/news-releases/neuron-ev-releases-teaser-for-second-annual-ciie-exhibition-300932773.html" target="_blank">https://www.prnewswire.com/news-releases/neuron-ev-releases-teaser-for-second-annual-ciie-exhibition-300932773.html</a></p></li>
                            <li><p><a href="https://www.prnewswire.com/news-releases/neuron-ev-announces-exclusive-preview-of-product-line-300921424.html" target="_blank">https://www.prnewswire.com/news-releases/neuron-ev-announces-exclusive-preview-of-product-line-300921424.html</a></p></li>
                            <li><p><a href="https://www.prnewswire.com/news-releases/neuron-ev-expands-the-renewable-energy-transportation-landscape-300907430.html" target="_blank">https://www.prnewswire.com/news-releases/neuron-ev-expands-the-renewable-energy-transportation-landscape-300907430.html</a></p></li>
                            <li><p><a href="https://www.prnewswire.com/news-releases/neuron-ev-steers-into-the-future-with-new-electric-bus-300898554.html" target="_blank">https://www.prnewswire.com/news-releases/neuron-ev-steers-into-the-future-with-new-electric-bus-300898554.html</a></p></li>
                            <li><p><a href="https://www.prnewswire.com/news-releases/neuron-ev-unveils-the-future-of-smart-travel-and-residence-300880495.html" target="_blank">https://www.prnewswire.com/news-releases/neuron-ev-unveils-the-future-of-smart-travel-and-residence-300880495.html</a></p></li>
                            <li><p>牛仁电动汽车将于2019年第四季度公布更多未来车型信息。</p></li>
                            <li><p><a href="https://www.prnewswire.com/news-releases/neuron-ev-unveils-hub-a-new-paradigm-of-transportation-300847903.html?tc=eml_cleartime" target="_blank">https://www.prnewswire.com/news-releases/neuron-ev-unveils-hub-a-new-paradigm-of-transportation-300847903.html?tc=eml_cleartime</a></p></li>
                            <li><p>牛仁电动汽车发布了智能交通未来“HUB”的预览。</p></li>
                            <li><p>牛仁电动汽车启动自动交通系统的概念研究并创立了“HUB”</p></li>
                            <li><p>牛仁电动汽车启动概念车的生产研究。</p></li>
                            <li><p>牛仁电动汽车启动电动汽车的发展。</p></li>
                            <li><p>DSMO启动了移动产品的生产研究。</p></li>
                            <li><p>DSMO启动智能移动解决方案的开发。</p></li>
                            <li><p>牛仁公司成立了一家移动解决方案公司DSMO（多功能智能移动设备）。</p></li>
                            <li><p>牛仁公司推出了牛仁电动汽车，这是一家具有远见卓识的汽车公司，致力于 创新下一代电动汽车。 </p></li>
                        </ul>
                    </div>
                    <div className="calender-conatiner">
                        <Calender />
                    </div>
                    <Footer />
                </main>
            </div>
        )
    }
}

export default Press