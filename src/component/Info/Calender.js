import React from 'react';

let intervalVal;

class Calender extends React.Component {
    componentDidMount(){
        window.YUI().use('calendar', 'datatype-date', 'cssbutton',  function(Y) {
            var calendar = new Y.Calendar({
              contentBox: "#mycalendar",
              date: new Date()}).render();
        
            var dtdate = Y.DataType.Date;
        
            calendar.on("selectionChange", function (ev) {
              var newDate = ev.newSelection[0];
              if(Y.one("#selecteddate"))
              Y.one("#selecteddate").setHTML(dtdate.format(newDate));
            });
        });


        intervalVal = setInterval(() =>{
            let lastRow = document.getElementsByClassName('yui3-calendar-row').length;
            if(lastRow > 0 && document.getElementsByClassName('yui3-calendar-row')){                
                document.getElementsByClassName('yui3-calendar-row')[lastRow - 1].style.display = 'none';
                clearInterval(intervalVal)
            }
        }, 1000)
    }

    render() {
        return (
                <div id="demo" className="yui3-skin-night yui3-g">
                    <div id="leftcolumn" className="yui3-u">
                        <div id="mycalendar"></div>
                    </div>
                </div>
        )
    }
}

export default Calender