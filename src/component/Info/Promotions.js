import React, { Component } from 'react';
import SocialIcon from '../SocialIcon';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Promotions extends Component {
    componentDidMount(){
        document.title= "PROMO — Neuron EV";
    }
    render() {
        return (
            <div className="promotion-container">
                <RespHeader />
                <main className="star-main-wrapper">
                <Header />
                <div className="promotion-wrapper">
                    <h2>促销活动</h2>
                </div>
                <div className="promotion-content-wrapper">
                    <h2 className="first-child"> 促销规则和指南</h2>
                    {/* <p> NO PURCHASE IS NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT INCREASE THE CHANCES OF WINNING.</p> */}
                    {/* <p>1. Eligibility: Neuron Corporation Campaigns are open only to those who sign up at a Neuron EV
                        generated website and who are 13 or older as of the date of entry. Neuron EV Campaigns are currently only open to legal residents of US and is void where prohibited by law.
                        Employees of Neuron Corporation, its affiliates, subsidiaries, advertising and promotion agencies, and suppliers, (collectively the “Employees”), and
                        immediate family members and/or those living in the same household of Employees are not eligible to participate in the Campaign.
                        The Campaign is subject to all applicable federal, state, and local laws and regulations. Void where prohibited.</p>
                    <p>2. Agreement to Rules: By participating, the Contestant (“You”) agree to be fully unconditionally
                         bound by these Rules, and You represent and warrant that You meet the eligibility requirements.
                         In addition,
                         You agree to accept the decisions of Neuron Corporation as final and binding as it relates to
                         the content of this Campaign.</p>
                    <p>3. Campaign Period: Entries will be accepted online starting on the date indicated and ending
                        the date indicated on each promotion.
                        All online entries must be received by the date indicated on each promotion.</p> */}
                    {/* <p>4. How to Enter: The Campaign must be entered by submitting an entry using the
                         online form provided at a Neuron Corporation website. The entry must fulfill all Campaign
                         requirements, as specified, to be eligible to win a prize.
                         Entries that are incomplete or do not adhere to the rules or specifications may be disqualified at
                          the sole discretion of Neuron Corporation. Optional verbiage to include: You may enter only once. You must provide the information requested.
                           You may not enter more times than indicated by using multiple email addresses, identities, or devices in an attempt to circumvent the rules.
                           If You use fraudulent methods or otherwise attempt to circumvent the rules, your submission may be removed from eligibility at the sole discretion of Neuron Corporation.</p> */}

                    {/* <p> <strong>Promotion Rules and Guidelines</strong></p> */}
                    <p>无需购买即可参与或赢取。购买不会增加赢取的机会。</p>
                    <p>1. 资格：牛仁公司竞选活动仅向在牛仁电动汽车生成的网站上注册且自入境之日起年满13周岁的人士开放。牛仁电动汽车活动目前仅向美国合法居民开放，在法律禁止的地方无效。牛仁公司的员工，其分支机构，子公司，广告和推广机构以及供应商（统称为“员工”）以及直系亲属和/或住在同一员工家庭中的人员均无资格参加此次运动。该运动受所有适用的联邦，州和地方法律和法规的约束。 禁止的地方无效。</p>
                    <p>2. 遵守规则：参赛者（“您”）同意完全无条件地受这些规则的约束，并且您声明并保证您符合资格要求。 此外，您同意接受牛仁公司的最终决定，因为它与本活动的内容有关。</p>
                    <p>3.活动期限：从指定的日期开始到每个促销的指示的日期结束，都可以在线接受报名。 所有在线条目必须在每次促销中指定的日期之前收到。</p>
                    <p>4. 参与方式：必须通过使用牛仁公司网站上提供的在线表格提交登记来进入活动。参赛者必须符合规定的所有竞选要求，才有资格获奖。牛仁公司可自行决定取消不完整或不遵守规则或规范的条目的资格。可选的选项包括：您只能参与一次。 您必须提供所需的信息。您不能使用多个电子邮件地址、身份或设备来超过规定的次数，规避规则。 如果您使用欺诈性方法或以其他方式试图规避规则，那么牛仁公司可自行决定取消您的申请资格。</p>
                    <p>5. 奖品：活动的获胜者（以下简称“获胜者”）将获得每场促销活动上标明的价格。实际/评估价值在授予奖金时可能会有所不同。奖金的具体内容将由牛仁公司单独决定。除非牛仁公司自行决定，否则不得使用现金或其他奖品替代。奖品是不可转让的。任何和所有奖金相关的费用，包括但不限于任何和所有联邦、州和/或地方税，应由获奖者全权负责。获奖者不得以奖金代替或将奖金转让给他人或要求现金等价物。获奖构成允许牛仁公司使用获奖者的名字，肖像，并应用广告和贸易的目的，并且没有进一步的补偿，除非法律禁止。</p>
                    <p>6.几率：获奖的几率取决于所收到的合格参赛作品的数量。</p>
                    <p>7. 获奖者评选及通知：获奖者将在牛仁公司的监督下，通过随机抽签或社区投票的方式进行评选。获奖者将在获选后五（5）天内通过电子邮件、电话或数字信息通知获奖者。牛仁公司对因垃圾邮件或其他安全设置而未能收到通知，或对获奖者提供的不正确或其他不起作用的联系信息不承担任何责任。如果无法联系到获奖者、不符合资格、未能在获奖通知发出之日起5天内申领奖品、或未能按要求及时返回已完成并执行的申报和发放，则可没收奖品，并选出候补获奖者。获奖者在本次活动中获得奖金的条件是遵守任何及所有联邦、州和地方法律法规。获胜者违反这些官方规则（由牛仁公司全权决定）将导致获胜者被取消竞选获胜者的资格，所有获胜者的特权将立即终止。</p>
                    <p>8. 您授予的权利：通过参与（如照片、视频、文本等），您了解并同意Neu牛仁公司、任何代表牛仁公司行事的人，以及牛仁公司的被许可人、继承人和受让人，在法律允许的情况下，有权打印、出版、广播、分发，以及在目前已知的或以后发展的任何媒体中，可永久地和在全世界范围内使用，无任何限制，您的登记、姓名、肖像、图片、声音、肖像、形象、关于活动的声明，以及用于新闻、宣传、信息、贸易、广告、公共关系和促销目的的履历信息，无需任何进一步的补偿、通知、审查或同意。</p>
                    <p>9. 条款和条件：如果出现病毒，漏洞，未经授权的人为干预，欺诈或超出牛仁公司无法控制的破坏或影响管理，安全性，公平性，或适当活动进行的其他原因，牛仁公司保留自行决定取消，终止，修改或暂停活动的权利。在这种情况下，牛仁公司可从牛仁公司采取行动之前和/或之后（如适当）收到的所有合格参赛者中选出获胜者。牛仁公司有权自行决定取消任何篡改或试图篡改活动或网站的进入流程或操作或违反本条款和条件的个人的资格。牛仁公司有权自行决定维持活动的完整性，以任何理由取消投票，包括但不限于：来自同一用户的来自不同IP地址的多次参与；来自同一台计算机的多次参与超出了活动规则允许的范围；或者使用机器人程序、宏、脚本或其他技术手段进行参与。参赛者蓄意破坏任何网站或破坏活动合法运作的任何企图都可能违反刑事和民事法律。如有此意图，牛仁公司保留在法律允许的最大范围内寻求损害赔偿的权利。</p>
<p>10. 责任限制：通过参与，即表示您同意免除并保护牛仁公司及其子公司、附属公司、广告和促销机构、合作伙伴、代表、代理、继任者、受让人、雇员、管理人员和董事的任何责任、疾病、伤害、死亡、损失、诉讼、索赔，或可能直接或间接发生的损害，无论是否由疏忽造成，来自：i）参赛者参与活动和/或他/她接受、拥有、使用或滥用任何奖品或奖品的任何部分；（ii）任何种类的技术故障，包括但不限于任何计算机、电缆、网络、硬件或软件或其他机械设备的故障；（iii）任何传输、电话或互联网服务不可用或不方便用；（iv）未经授权的人为干预参与过程或促销过程；（v）在促销或处理参与审核中出现电子或人为错误。</p>
<p>11. 争议：这场活动受美利坚合众国和加利福尼亚州的法律管辖，不受法律冲突学说的影响。作为参与本活动的一个条件，参与者同意，双方之间无法解决的任何和所有争议，以及由本活动引起或与之相关的诉讼原因，应单独解决，而不诉诸任何形式的集体诉讼，专门由位于加利福尼亚的法院管辖。此外，在任何此类争议中，在任何情况下，均不得允许参与者获得赔偿，并在此放弃所有惩罚性、附带性或后果性损害的权利，包括合理的律师费，但参与者的实际自付费用（即与参与本活动相关的费用）除外。参与者进一步放弃成倍或增加损失的权利。</p>
<p>12. 隐私政策：牛仁公司主办的活动绝不是Facebook赞助、支持、管理或与之相关的。参与登记一起提交的信息受牛仁公司网站上声明的隐私政策的约束。要阅读隐私政策，请单击此处访问
    
<a className="promotion-link" href="https://www.neuronev.co/legal">https://www.neuronev.co/legal 。</a>
   </p>

                </div>
                <div className="promotion-social-icons promo-icon">
                    <SocialIcon />
                </div>
                <Footer/>
                </main>
            </div>
        )
    }
}

export default Promotions