import React, { Component } from 'react';
import ContactForm from './ContactForm';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';


class Contact extends Component {
    componentDidMount(){
        document.title= "CONTACT — Neuron EV";
    }
    render() {
        return (
            <>
            <div className="contact-response">
            <RespHeader />
            </div>
            <div>
                <Header />
            <div className="contact-conatiner mt-30">
                <div className="contact-title"> <h2> 联系我们 </h2></div>
                <div className="contact-form-container">
                        <ContactForm />
                </div>
            </div>
            <Footer/>
            </div>
            </>
        
        )
    }
}

export default Contact