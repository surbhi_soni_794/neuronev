import React, { Component } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';

class Faq extends Component {
    componentDidMount() {
        document.title = "FAQ — Neuron EV";
    }
    render() {
        return (

            <div className="vision-container">
                <RespHeader />
                <main className="star-main-wrapper">
                    <Header />
                    <div className="faq-section">
                        <h2>牛仁电动汽车常见问题解答</h2>
                        <p>如需了解更多牛仁电动汽车详情和关于我们面向未来可持续发展的动态。</p>
                        <p>请垂询至contact@neuronev.co。</p>
                        <h1>基本信息</h1>
                        <p>牛仁电动汽车的愿景是什么？</p>
                        <p>通过清洁能源交通运输方式促进人类发展。</p>
                        <p className="questions">“推动变革”的意义是什么？</p>
                        <p>是统一，团结统一实现未来的可持续发展。
                        </p>
                        <p className="questions">牛仁的产品理念是什么？</p>
                        <p>通过智能化技术来提升价值。</p>
                        <p className="questions">牛仁电动汽车于何时在何地发布了产品？</p>
                        <p>牛仁电动汽车于2019年11月5日在中国上海第二届中国国际进口博览会上发布了两款新产品T-ONE和TORQ。牛仁电动汽和将于12月16日至22日在洛杉矶拉·谢纳加大道（La Cienega Blvd ）819号西好莱坞举办品牌视觉分享展。展览门票免费，每天上午11点至晚上7点向公众开放。</p>
                        <p className="questions">牛仁电动汽车目前在哪里展览？</p>
                        <p>牛仁电动汽车目前在位于上海虹桥中心商务区的虹桥进口商品展示交易中心举办T-ONE和TORQ的展览。展览周一至周日上午10:00至下午5:00向公众开放。
                        </p>
                        <p className="questions">怎样获取汽车产品图片？</p>
                        <p>
                        您可访问公司官方网站www.neuronev.co获取牛仁电动汽车产品图片</p>

                        <p className="questions">为什么没有加大对牛仁电动汽车的宣传呢？</p>

                        <p>我们希望让更多的人通过体验牛仁汽车来提高我们的知名度。</p>
                        <p className="questions">牛仁电动汽车有哪些类型的动力转动系统？（全轮驱动、前轮驱动、后轮驱动或按需四轮驱动）</p>
                        <p>牛仁电动汽车根据车辆用途提供不同的传动系统，包括前轮驱动和全轮驱动。</p>
                        <p className="questions">从哪些渠道可以更深入地了解牛仁电动汽车的系列产品？</p>
                        <p>有关所有产品系列信息，请访问www.neuronev.co网站电动汽车部分。</p>
                        <h1>产品相关信息</h1>
                        <p>什么是REP？</p>
                        <p>
                        可再生能源广场（Renewable Energy Plaza，简称REP）是一个引人注目集技术与休闲于一体的服务中心，人们在这里一边享受休闲时光，一边为他们的电动汽车充电。。</p>
                        <p className="questions">REP以什么方式来提供能源？</p>
                        <p>REP以各种方式提供能源：太阳能电池板发电、燃料电池储存电能和发电机的无线快速充电。它还提供维护服务和电池更换服务。</p>
                        <p className="questions">REP的主要特点是什么？</p>

                        <p>该中心的特点是将食品、饮料和娱乐融合在一个有趣的虚拟购物中心内，通过带有直播购物的大型数字化屏幕进行浏览和交易。它有一个无人送货系统，人们可以在那里购物和接收货物，享受快捷服务。</p>
                        <p className="questions">MAP是什么的缩写？</p>
                        <p>
                        MAP是多功能汽车平台的缩写。在不久的将来，字母A也将代表自动化。</p>
                        <p className="questions">MAP是什么?</p>
                        <p>
                        MAP是一种安装各种尺寸结构的自动机动化平台，以便将它们从一个位置运输到另一个位置。</p>
                        <p className="questions">MAP的主要目标是什么？</p><p>
                        MAP的主要目标是使静态结构可移动，从而扩展其功能。它的设计与标准集装箱兼容。MAP集成集装箱可实现无限定制功能的自动驾驶空间。</p>

                        <p className="questions">MAP的内部可进行怎样的调整？</p>
                        <p>
                        MAP内部可调整为但不限于商店、咖啡馆、餐厅、酒店和办公室。</p>
                        <p className="questions">MAP的尺寸是多少？</p>
                        <p>
                        MAP是一个带有15英尺的箱式房子，长6,670毫米，宽2,600毫米，高3,540毫米。</p>
                        <p className="questions">HUB是什么的缩写？</p>
                        <p>
                        HUB是和谐、多功能、平衡的缩写。</p>
                        <p className="questions">HUB的愿景是什么？</p>
                        <p>
                        HUB的愿景是统一而与众不同，多样而不分散。HUB是一种新的交通运输模式，将人、技术和基础设施连接在一起。
                            </p>
                        <p className="questions">HUB可作为私家车进行预订吗？</p>
                        <p>
                        可以进行预订。用户可以选择基本、舒适和高级等不同的居住设施。“HUB”基本款可承载6名乘客，空间舒适，且货物空间充足。</p>
                        <p className="questions">HUB有哪些应用？</p>

                        <ul>
                            <li>货物运输</li>
                            <li>储物柜</li>
                            <li>急救车</li>
                            <li>商业空间</li>
                            <li>共享驾驶</li>
                            <li>住宿</li>
                            <li>穿梭服务</li>
                            <li>车队租凭</li>
                            <li>私家车</li>
                        </ul>
                        <p className="questions">电动公共汽车的长度是多少？</p>
                        <p>
                        电动公共汽车是完全模块化的，这意味着车辆的长度可以改变，以多种方式来提供服务，可采用不同交通运输形式，如小型公交车、公共巴士或铰接式客车，可满足社区和基础设施的多功能用途。</p>
                        <p className="questions">牛仁电动公共汽车的创新性在哪里？</p>
                        <p>
                        牛仁电动公共汽车是模块化的，可与高速铁路兼容，本质上是一种可以转换成快速地铁列车的客车。</p> <p>
                        </p> <p className="questions">MET是一款什么样的车？</p>
                        <p>
                        MET是一款中型电动汽车。</p>
                        <p className="questions">MET后部可替换吗？</p>
                        <p>
                        MET的后部是可互换的，适用于城市物流、卫生、建筑和采矿等多种功能领域当中。车辆的结构支撑刚性和铰接卡车框架，为商业应用提供灵活的平台。
                            </p>
                        <p className="questions">STAR是一款什么样的车？</p>
                        <p>
                        牛仁电动汽STAR车是一款多功能微型卡车。它是一款电动敞篷卡车，后部可互换，以优化多功能性。</p>
                        <p className="questions">牛仁T-ONE是一款什么样的车？</p>
                        <p>
                        T-ONE是公司创新的多功能一体式EUV（电动多功能车），用于商业和个人用途。 

</p>
                        <p className="questions">T-ONE的独特性特点是什么？
</p><p>
T-ONE的特点是采用模块化平台、可升级技术、多功能货物功能、一流的内饰体验和标志性的空气动力学。这些车辆是为了减少浪费，并为保护环境提出可再生的解决方案。</p>
                        <p className="questions">T-ONE和TORQ的车身组件是可拆卸的吗？
</p><p>
基本的拖拉机单元有一个可伸缩的底盘，带有可互换的车身部件，易于安装和拆卸，便于改变其外观和功能。
</p>
                        <p className="questions">T-ONE动力来自哪里？
</p><p>
T-ONE的多源推进系统从全电力牵引电池组、可更换备用电源和可拆卸太阳能电池板卡车床罩中获取电力。</p>
                        <p className="questions">T-ONE的详情有哪些？</p>
                        <p><strong>尺寸</strong></p>
                        <p>T-ONE相当于一款中型卡车。</p>
                        <p><strong>推动力 </strong>
                        </p><p>

                        T-ONE是一款纯电池电动汽车。</p>
                        <p><strong> 性能  </strong>
                        </p>
                        <ul>
                            <li>零排放
                            </li>
                            <li>啮合驱动动力学</li>
                            <li>范围广泛</li>
                            <li>节能</li>
                        </ul>
                        <p>随着时间的推移，电动汽车的性能在日常运行中已实现标准化。牛仁电动汽车致力于打破性能静态，包括经济效益、空间体验、智能技术和社会责任。
</p>
                        <p><strong>扩展力</strong></p>

                        <p>T-ONE平台是模块化的，可以实现各种形式和大小的功能。</p>
                        <p><strong>氢燃料电池性能</strong></p>
                        <p>T-ONE平台是一个开放的、可互换的推进系统。</p>
                        <p className="questions">T-ONE的价格区间是多少？
</p><p>
T-ONE有多款车身类型，因此定价范围很广。
</p><p className="questions">
T-ONE符合欧洲的路况条件吗？
</p><p>
T-ONE是一款全球通用汽车。</p>
                        <p className="questions">牛仁TORQ是一款什么样的车？</p><p>

                        牛仁 TORQ是一款电动半挂车，致力于开发商业运输的下一代解决方案。
</p><p className="questions">
TORQ的独特性特点是什么？
</p><p>
TORQ的特点是采用模块化平台、可升级技术、多功能货物功能、一流的内饰体验和标志性的空气动力学。这些车辆是为了减少浪费，并为保护环境提出可再生的解决方案。

</p>

                        <p className="questions">TORQ的车身组件是可拆卸的吗？</p><p>

                        基本的拖拉机单元有一个可伸缩的底盘，带有可互换的车身部件，易于安装和拆卸，便于改变其外观和功能。
</p><p className="questions">
TORQ动力来自哪里？
</p><p>
TORQ的多源推进系统从全电力牵引电池组、可更换备用电源和可拆卸太阳能电池板卡车床罩中获取电力。
</p><p className="questions">
TORQ的详情有哪些？</p>

                        <p><strong> 尺寸 </strong></p>
                        <p>
                        TORQ是一款8级商用卡车。
</p>
                        <p><strong> 推动力 </strong></p>
                        <p>
                        TORQ是一款纯电池电动汽车。


</p>
                        <p><strong> 性能 </strong></p>
                        <ul>
                            <li>零排放</li>
                            <li>
                            啮合驱动动力学</li>
                            <li>
                            范围广泛</li><li>
                            节能</li>
                        </ul>
                        <p>随着时间的推移，电动汽车的性能在日常运行中已实现标准化。牛仁电动汽车致力于打破性能静态，包括经济效益、空间体验、智能技术和社会责任。</p>
                        <p><strong>扩展力</strong></p>
                        <p>TORQ平台是模块化的，可以实现各种形式和大小的功能。</p>
                        <p><strong>氢燃料电池性能</strong></p>
                        <p>TORQ平台是一个开放的、可互换的推进系统。
                        </p>
                        <p className="questions">TORQ的价格区间是多少？</p><p>
                        TORQ有不同的级别，因此定价范围很广。</p>
                    </div>
                    <Footer />
                </main>
            </div>
        )
    }
}

export default Faq