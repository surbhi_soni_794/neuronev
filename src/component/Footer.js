import React,{Component} from 'react';
import SocialIcon from './SocialIcon';

class Footer extends Component{
   render(){
     return(
    <div id="footerSection">
  
   <footer className="footer">
    <div className="inner-footer">
      <div className="empty-block-footer">
        <div className="my-own-row">
        <div className="col sqs-col-12 span-12"></div>
        </div>
      </div>
     
        <nav className="footer-nav">
          <div className="footer-group-section">
            <span className="fotter-group-items">关注我们</span>
            <div className="social-icons my-own-social-icons own-social-links">
           <SocialIcon/>
          </div>
          </div>
        </nav>
      <div className="footer-block">
        <div className="row-aside">
          <div className="col-12 col-sm-12">
            <div className="social-icons"></div>
          </div>
        </div>
      </div>
      <nav/>
      <div className="block-section">
       <div className="block-content">
       <div className="jumbotron text-center my-own-text change-color">
       <h2 className="newsletter-block-title ">订阅</h2>
            <p className="newsletter-para-description">登记您的电子邮箱以接收新闻和更多更新信息。</p>
        <div className="subscribe-form-group">
        <div className="email-container">
             <div className="email-section">
              <input type="text" name="email" placeholder="邮箱地址" className="newsletter-form-field-element field-element">
                </input> 
             </div>
             <div className="button-section">
             <div className="submit-wrapper">
        <button className="newsletter-form-button" type="submit" value="Sign Up">
          <span className="newsletter-form-spinner sqs-spin light large"></span><span className="newsletter-form-button-label">登记</span><span className="newsletter-form-button-icon"></span></button>
      </div>
             </div>
            
          </div>
        </div>
        <div className="newsletter-form-footnote"><p>我们会尊重您的隐私</p></div>
      
          </div>
   <div>
     <div className="footer-section-container">
     <h3 className="footer-copyright">版权所有©2019牛仁电动汽车公司 </h3>
     </div>
   </div>
       </div>
      </div>
    </div>
  </footer> 
    </div>
   
     )
   }
} 

export default Footer;
