import React, { Component } from 'react';
import SocialIcon from './SocialIcon';
import { withRouter } from 'react-router-dom';
import { changeRoute } from '../store/actions/index';

class Header extends Component {

  redirectTo = () => {
    changeRoute('/')
    this.props.history.push('/')
  }

  redirectToStar = () => {
    changeRoute('/star')
    this.props.history.push('/star')
  }

  redirectToMet = () => {
    changeRoute('/met')
    this.props.history.push('/met')
  }

  redirectToEbus = () => {
    changeRoute('/about')
    this.props.history.push('/about')
  }

  redirectToHub = () => {
    changeRoute('/new-index')
    this.props.history.push('/new-index')

  }
  redirectToRep = () => {
    changeRoute('/rep')
    this.props.history.push('/rep')

  }
  redirectToMap = () => {
    changeRoute('/map')
    this.props.history.push('/map')
  }

  redirectToOne = () => {
    changeRoute('/new-page-7')
    this.props.history.push('/new-page-7')
  }

  redirectToFaq = () => {
    changeRoute('/faq')
    this.props.history.push('/faq')
  }

  redirectToABOUT = () => {
    changeRoute('/vision')
    this.props.history.push('/vision')
  }
  redirectToSocial = () => {
    changeRoute('/social')
    this.props.history.push('/social')
  }
  redirectToTech = () => {
    changeRoute('/tech')
    this.props.history.push('/tech')
  }
  redirectToPromo = () => {
    changeRoute('/promotions')
    this.props.history.push('/promotions')
  }
  redirectToEvent = () => {
    changeRoute('/press')
    this.props.history.push('/press')
  }
  redirectToLegal = () => {
    changeRoute('/legal')
    this.props.history.push('/legal')
  }
  redirectToContact = () => {
    changeRoute('/contact')
    this.props.history.push('/contact')
  }

  redirectToTorq = () => {
    changeRoute('/torq')
    this.props.history.push('/torq')
  }
  
  redirectToRoute = (routeName) => {
    changeRoute('pressSection')
    this.props.history.push(routeName)
  }
  render() {
    return (
      <div>
        <section className="header-section">
          <nav className="navbar navbar-expand-md navbar-light my-own-navbar own-navs">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <span className="navbar-brand" onClick={this.redirectTo}> <img alt="logo" className="img-branding-logo" src="http://cdn.cn.neuronev.co/assets/images/logo.png" /></span>

            <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
              <ul className="">
                <li className="nav-item active">
                  <span className="nav-link" onClick={this.redirectTo}>牛仁</span>
                </li>
                <li className="nav-item dropdown">
                  <span className="nav-link" >电动汽车</span>
                  <div className="dropdown-content" >
                    <span onClick={this.redirectToStar}>Star</span>
                    <span onClick={this.redirectToMet}>Met</span>
                    <span onClick={this.redirectToEbus}>Ebus</span>
                    <span onClick={this.redirectToMap}>Map</span>
                    <span onClick={this.redirectToHub}>HUb</span>
                    <span onClick={this.redirectToRep}>Rep</span>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <span className="nav-link">预定</span>
                  <div className="dropdown-content" >
                    <span onClick={this.redirectToOne}>T.ONE</span>
                    <span onClick={this.redirectToTorq}>TORQ</span>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <span className="nav-link" >品牌信息</span>
                  <div className="dropdown-content" >
                  <span onClick={this.redirectToFaq}>FAQ</span>
                    <span onClick={this.redirectToABOUT}>关于我们</span>
                    <span onClick={this.redirectToSocial}>社会</span>
                    <span onClick={this.redirectToTech}>科技</span>
                    <span onClick={this.redirectToPromo}>促销活动</span>
                    <span onClick={this.redirectToEvent}>大事件</span>
                    <span onClick={this.redirectToLegal}>法律</span>
                    <span onClick={this.redirectToContact}>联系我们</span>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <span className="nav-link" >新闻中心</span>
                  <div className="dropdown-content" >
                    <span onClick={() => this.redirectToRoute('/121019')}>12/10/19</span>
                    <span onClick={() => this.redirectToRoute('/12-06-19')}>12/06/19</span>
                    <span onClick={() => this.redirectToRoute('/questionsandanswers')}>11/22/19</span>
                    <span onClick={() => this.redirectToRoute('/ciie-2019')}>11/20/19</span>
                    <span onClick={() => this.redirectToRoute('/evintro')}>11/06/19</span>
                    <span onClick={() => this.redirectToRoute('/expo3')}>10/21/19</span>
                    <span onClick={() => this.redirectToRoute('/expo2')}>10/07/19</span>
                    <span onClick={() => this.redirectToRoute('/starpress')}>10/01/19</span>
                    <span onClick={() => this.redirectToRoute('/metpress')}>10/01/19</span>
                    <span onClick={() => this.redirectToRoute('/vision-press')}>09/19/19</span>
                    <span onClick={() => this.redirectToRoute('/new-index-2')}>08/27/19</span>
                    <span onClick={() => this.redirectToRoute('/ebus_press')}>08/08/19</span>
                    <span onClick={() => this.redirectToRoute('/mappress')}>07/08/19</span>
                    <span onClick={() => this.redirectToRoute('/hubpress')}>05/10/19</span>
                  </div>
                </li>
              </ul>
              <div className="social-icons my-own-social-icon">
                <SocialIcon />
              </div>
            </div>
          </nav>
        </section>
      </div>
    )
  }
}

export default withRouter(Header);
