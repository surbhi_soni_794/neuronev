import React, { Component } from 'react';
import {slidingImages} from '../../store/actions/index'
import $ from 'jquery';
import Header from '../Header';
import Footer from '../Footer';
import RespHeader from '../RespHeader';


class Home extends Component {
  constructor(props){
    super(props);
    $(document).ready(() => {
      let duration = 12;
      let fadeAmount = 0.2;
      let durationTime = 1200;
      slidingImages(duration, fadeAmount, durationTime)
    })
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, true);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () =>{
  }
  

  goToFooter =() => {
    var elmnt = document.getElementById("footerSection");
    elmnt.scrollIntoView();
  }

  componentDidMount(){
    document.title= "Neuron EV";


// this.carousel()
  }
   


  render() {
    return (
        <div className="side-color">
          <RespHeader />
           <main className="star-main-wrappers">
                <Header />
          <div className="slide-img">
            <div  id="slideshow" className="sliding-images mt-60 morpher-image">
              <img alt="home-A" src="http://cdn.cn.neuronev.co/assets/images/home/A.jpg"/>
              <img alt="home-B" src="http://cdn.cn.neuronev.co/assets/images/home/B.jpg"/>
              <img alt="home-C" src="http://cdn.cn.neuronev.co/assets/images/home/C.jpg"/>
              <img alt="home-D" src="http://cdn.cn.neuronev.co/assets/images/home/D.jpg"/>
              <img alt="home-Neuron_IMG652" src="http://cdn.cn.neuronev.co/assets/images/home/Neuron_IMG652.jpeg" />
              <img alt="home-Neuron_IMG653" src="http://cdn.cn.neuronev.co/assets/images/home/Neuron_IMG653.jpeg" />
              <img alt="home-Neuron_IMG654" src="http://cdn.cn.neuronev.co/assets/images/home/Neuron_IMG654.jpeg" />
            </div>
            <section className="experience-section">
         <div className="experience -inner-section">
          <div className="experience-section">
            <div className="container-fluid">
              <div className="col-12 col-sm-12 col-lg-12 col-xs-12">
              <h2 className="experience-heading text-wrapper"> 体验牛仁电动汽车</h2>
              <h1 className="experience-another-heading">世界上第一家电动多功能汽车公司</h1>
              </div>
              <div className="col-12 col-sm-12 col-lg-12 col-xs-12">
              <div className="block-content home-bottom-container">
                <h2 className="neuron-ev-heading">全新NEURON EV展览现已开幕</h2>
                
                <h2 className="neuron-ev-heading">虹桥国际展汇<br></br>申昆路2377号<br></br>上海市闵行区<br></br>
                星期一至日10:00 - 17:00  
                </h2>
                  </div>
                  <div onClick={this.goToFooter} className="arrow-image-my-own-img">
                    <div className="set-index-arrow-home">
                    <img id="drop-down-images" alt="down-img" src="http://cdn.cn.neuronev.co/assets/images/down-3.png" />
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div> 
        </section>


          </div>
          <Footer/>
        </main>
        </div>
    )
  }
}

export default Home